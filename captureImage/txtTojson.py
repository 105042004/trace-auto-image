import xml.etree.ElementTree as ET
import json


def GPS_Path(file_path) :
    text_file = open(file_path, "r")
    lines = text_file.readlines()
    Path_JSON = {}
    Path_ARRAY = []
    headingOutput = ''
    for _line in lines:
        grid_node = {}
        vector_line = _line.split(',')
        gps_node = []
        gps_node.append(float(vector_line[0])/100)
        gps_node.append(float(vector_line[1])/100)
        grid_node['lat'] = gps_node[0]
        grid_node['lon'] = gps_node[1]
        Path_ARRAY.append(grid_node)
        headingOutput += vector_line[2]

    Path_JSON['path'] = Path_ARRAY

    with open('D:/AutoImage/data/model_filled_afterUnreal/temp_after/20190912_path.json', 'w') as outfile:
        json.dump(Path_JSON, outfile)
    with open('D:/AutoImage/data/model_filled_afterUnreal/temp_after/20190912_heading.txt', 'w') as file:
        file.write(headingOutput)


GPS_Path('D:/AutoImage/data/model_filled_afterUnreal/temp_after/position_Fixed_LS.txt')
