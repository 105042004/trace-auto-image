import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
import CalculateHogResult
import CalculateFloorRatioVideoResult
import CalculateScoreVideoResult
import lineResult
from os import listdir
from os.path import isfile, isdir, join

class positionHeadingData():
 def __init__(self,x,y,heading):
  self.x = x
  self.y = y
  self.heading = heading
  self.score = -1
  self.smoothScore = -1



class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.HogScore = 5000
  self.SegScore = -1
  self.x = 0
  self.y = 0
  self.heading = 0


def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2


if __name__ == '__main__':
    #inputPath = 'F:/straight/temp_0527_circle/groundTruth/result.txt'
    #resultPath = 'F:/straight/temp_0527_circle/method1/result.txt'
    #finialScorePath = 'F:/straight/temp_0527_circle/data/finialScore/'
    #fileNamePath = "F:/straight/temp_0527_circle/data/FileName/"
    outPath = 'D:/lab/ITRI/demo/20190426/demo1/'
    inputTotalPath = outPath + 'groundTruth/'
    resultTotalPath = outPath+'smoothTerm/'
    inputPath = outPath + 'groundTruth/position_lineFitting.txt'
    resultPath = resultTotalPath+'position_lineFitting.txt'
    inputHeadingPath = outPath + 'groundTruth/position_lineFitting_heading.txt'
    resultHeadingPath = resultTotalPath +  'position_lineFitting_heading.txt'
    finialScorePath = outPath + 'temp/data/finialScore/'
    fileNamePath = outPath + "temp/data/FileName/"
    startIndex = 1000
    endIndex = -1
    files = listdir(finialScorePath)
    TotalScore = []
    groundTruthResult = []
    finialResult = []
    for i in range(len(files)+420):
        TotalScore.append([])
        groundTruthResult.append(positionHeadingData(-1,-1,-1))
        finialResult.append(positionHeadingData(-1,-1,-1))
        for j in range(201) :
            TotalScore[i].append([])
            for k in range(41) :
                TotalScore[i][j].append(Score(500))
    
    
    totalScoreSum = 0
    totalSumNumber = 0
    ScorePath = open(inputPath, 'r')
    index = 0
    lineResult.lineGenerator(inputTotalPath + 'image/',inputTotalPath+'line/')
    for lines in ScorePath:
         arr= lines.split(',')
         groundTruthResult[index].x = float(arr[0])
         groundTruthResult[index].y = float(arr[1])
         CalculateHogResult.calculateHOG(inputTotalPath+'line/',outPath + 'finialLine/',inputTotalPath+'HOG/',index)
         CalculateFloorRatioVideoResult.run(inputTotalPath + 'image/',outPath+'Seg/',inputTotalPath+'Seg/',index)
         CalculateScoreVideoResult.run(outPath,inputTotalPath+'Seg/',inputTotalPath+'HOG/',inputTotalPath+'finialScore/',index,0)
         gdScore  = open(inputTotalPath+'finialScore/'+str(index)+'.txt', 'r')
         for gdline in gdScore:
            arr = gdline.split(',')
            groundTruthResult[index].score = float(arr[1])
         if index < startIndex:
             startIndex = index
         
         if index > endIndex:
             endIndex = index
         #groundTruthResult[index].score = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].score
         index = index+1
         totalSumNumber = index

    HeadingPath = open(inputHeadingPath, 'r')
    index = 0
    for lines in HeadingPath:
         groundTruthResult[index].heading = float(lines)
         index = index+1
    
    for i in range(startIndex+1,endIndex-1):
        lat = groundTruthResult[i].x
        lon = groundTruthResult[i].y
        nextLat = groundTruthResult[i+1].x
        nextLon = groundTruthResult[i+1].y
        preLat = groundTruthResult[i-1].x
        preLon = groundTruthResult[i-1].y
        dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
        dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
        heading = abs(groundTruthResult[i+1].heading -groundTruthResult[i].heading)
        heading += abs(groundTruthResult[i].heading - groundTruthResult[i-1].heading)
        scoreTotal = groundTruthResult[i].score +groundTruthResult[i+1].score +groundTruthResult[i-1].score
        disScore = dealScore(dis1+dis2,300)
        headingScore = dealScore(heading,15)
        Score = dealScore(scoreTotal,0.7)
        finialSmoothScore = 0.3*(0.5*disScore + 0.5*headingScore) + 0.7*Score
        totalScoreSum += finialSmoothScore
        print(i,finialSmoothScore)
    totalSumNumber2 = 0
    totalScoreSum2 = 0
    ScorePath = open(resultPath, 'r')
    index = 0
    startIndex = 1000
    endIndex = -1
    lineResult.lineGenerator(resultTotalPath + 'image/',resultTotalPath+'line/')
    for lines in ScorePath:
         arr= lines.split(',')
         finialResult[index].x = float(arr[0])
         finialResult[index].y = float(arr[1])
         CalculateHogResult.calculateHOG(resultTotalPath+'line/',outPath + 'finialLine/',resultTotalPath+'HOG/',index)
         CalculateFloorRatioVideoResult.run(resultTotalPath + 'image/',outPath+'Seg/',resultTotalPath+'Seg/',index)
         CalculateScoreVideoResult.run(outPath,resultTotalPath+'Seg/',resultTotalPath+'HOG/',resultTotalPath+'finialScore/',index,0)
         gdScore  = open(resultTotalPath+'finialScore/'+str(index)+'.txt', 'r')
         for gdline in gdScore:
            arr = gdline.split(',')
            finialResult[index].score = float(arr[1])
         if index < startIndex:
             startIndex = index
         
         if index > endIndex:
             endIndex = index
         #finialResult[index].score = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].score
         index = index+1


    HeadingPath = open(resultHeadingPath, 'r')
    index = 0
    for lines in HeadingPath:
         finialResult[index].heading = float(lines)
         index = index + 1
         totalSumNumber2 =  index

    for i in range(startIndex+1,endIndex-1):
        lat = finialResult[i].x
        lon = finialResult[i].y
        nextLat = finialResult[i+1].x
        nextLon = finialResult[i+1].y
        preLat = finialResult[i-1].x
        preLon = finialResult[i-1].y
        dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
        dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
        heading = abs(finialResult[i+1].heading -finialResult[i].heading)
        heading += abs(finialResult[i].heading - finialResult[i-1].heading)
        scoreTotal = finialResult[i].score +finialResult[i+1].score +finialResult[i-1].score
        disScore = dealScore(dis1+dis3,300)
        headingScore = dealScore(heading,15)
        Score = dealScore(scoreTotal,0.7)
        finialSmoothScore = 0.3*(0.5*disScore + 0.5*headingScore) + 0.7*Score
        totalScoreSum2 += finialSmoothScore
        print(i,finialSmoothScore)
    
    print(totalSumNumber,totalSumNumber2)
    print(totalScoreSum/totalSumNumber,totalScoreSum2/totalSumNumber2)



         