import cv2
import numpy as np
import glob
from os import listdir
from os.path import isfile, isdir, join
 
demo = 'demo11'
img_array = []
path = 'D:/lab/ITRI/demo/20190426/'+demo+'/method1/image_LS/result/'
files = listdir(path)
for filename in range(1,len(files)):
    print(path+str(filename)+'.png')
    img = cv2.imread(path+str(filename)+'.png')
    height, width, layers = img.shape
    size = (width,height)
    img_array.append(img)
 
 
out = cv2.VideoWriter('D:/lab/ITRI/demo/20190426/'+demo+'/method1/image_LS/'+demo+'.mp4',cv2.VideoWriter_fourcc(*'DIVX'), 8, size)
 


for i in range(len(img_array)):
    out.write(img_array[i])
out.release()
