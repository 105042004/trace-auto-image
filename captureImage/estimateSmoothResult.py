import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join
maxRange = 200
minRange = 0
finialSortScore = []
output = ''
class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.HogScore = 5000
  self.SegScore = -1
  self.x = 0
  self.y = 0
  self.heading = 0

class Sort():
   def __init__(self,index,offset,heading):
       self.index = index
       self.offset = offset
       self.heading = heading 

class frameDis():
     def __init__(self,offset,heading,nextOffset,nextHeading,preOffset,preHeading,dis,headingDis,score):
       self.offset = offset
       self.heading = heading
       self.nextOffset = nextOffset
       self.nextHeading = nextHeading
       self.preOffset = preOffset
       self.preHeading = preHeading
       self.dis = dis
       self.headingDis = headingDis
       self.score = score
       self.finialScore = -1

def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2

def method2(TotalScore,finialSortScore):
    minValueIndex = []
    for i in range(0,len(TotalScore)):
      minScore = 100000
      minOffset = -1
      minHeading = -1
      for j in range(0,len(TotalScore[i])):
          if TotalScore[i][j][20].score < minScore and j>= minRange and j<= maxRange:
                  minScore = TotalScore[i][j][20].score
                  minOffset = j
                  minHeading = 20
      minValueIndex.append(minOffset)

    for i in range(0,len(minValueIndex)):
        minScore = 10000
        minHeading = -1
        for j in range(0,len(TotalScore[i][minValueIndex[i]])):
             if TotalScore[i][minValueIndex[i]][j].score < minScore:
                  minScore = TotalScore[i][minValueIndex[i]][j].score
                  minHeading = j
        #print(i,minValueIndex[i]-100,minHeading-20,minScore)
        finialSortScore[i].append(Sort(i,minValueIndex[i],minHeading))
        #print(finialSortScore[i][ len(finialSortScore[i])-1 ].offset )
        for j in range(0,len(TotalScore[i][minValueIndex[i]])):
            TotalScore[i][minValueIndex[i]][j].finialScore = TotalScore[i][minValueIndex[i]][j].score
            TotalScore[i][minValueIndex[i]][j].score = 5000
    return TotalScore,finialSortScore

def method13(TotalScore,finialSortScore):
    minValueIndex = []
    
    for i in range(0,len(TotalScore)-1):
        allHeadingOffset = []
        minValueCount = []
        for o in range(41):
            allHeadingOffset.append(0)
            minValueCount.append(0)

        print(i)
        for j in range(0,len(TotalScore[i])):
            for k in range(0,len(TotalScore[i][j])):
                if TotalScore[i][j][k].score < 1 and j>= minRange and j<= maxRange:
                   #print(TotalScore[i][j][k].score)
                   allHeadingOffset[k] += TotalScore[i][j][k].score
                   minValueCount[k] = minValueCount[k]+1

        for j in range(0,len(allHeadingOffset)):
            if(allHeadingOffset[j] == 0):
                allHeadingOffset[j] = 10000
            else:
                allHeadingOffset[j] = allHeadingOffset[j]/minValueCount[j]
                print(j-100,allHeadingOffset[j])

        minValueIndex.append(allHeadingOffset.index(min(allHeadingOffset)))


    for i in range(0,len(minValueIndex)):
        minScore = 10000
        minOff = -1
        for j in range(0,len(TotalScore[i])):
            if TotalScore[i][j][minValueIndex[i]].score < minScore and j>= minRange and j<= maxRange:
                minScore = TotalScore[i][j][minValueIndex[i]].score
                minOff = j
        minScore = 10000
        minHeading = -1
        for k in range(0,len(TotalScore[i][minOff])):
            if TotalScore[i][minOff][k].score < minScore and j>= minRange and j<= maxRange:
                 minScore = TotalScore[i][minOff][k].score
                 minHeading = k
        finialSortScore[i].append(Sort(i,minOff,minHeading))
        for j in range(0,len(TotalScore[i][minOff])):
            TotalScore[i][minOff][j].finialScore = TotalScore[i][minOff][j].score
            TotalScore[i][minOff][j].score = 5000
        print(i,minOff-100,minHeading-20,minScore) 
    return TotalScore,finialSortScore    

def method1(TotalScore,finialSortScore):
    for i in range(0,len(TotalScore)):
      minScore = 100000
      minOffset = -1
      minHeading = -1
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              if TotalScore[i][j][k].score < minScore and j>=minRange and j<=maxRange:
                  minScore = TotalScore[i][j][k].score
                  minOffset = j
                  minHeading = k
      finialSortScore[i].append(Sort(i,minOffset,minHeading))
      TotalScore[i][minOffset][minHeading].finialScore = TotalScore[i][minOffset][minHeading].score
      TotalScore[i][minOffset][minHeading].score = 5000
      #print(i,minOffset-100,minHeading-20,minScore)
    return TotalScore,finialSortScore
      #print(TotalScore[i][minOffset][minHeading].fileName)

if __name__ == '__main__':
  TotalScorePath = "F:/straight/temp/data/finialScore/"
  fileNamePath = "F:/straight/temp/data/FileName/"
  files = listdir(TotalScorePath)
  TotalScore = []
  for i in range(len(files)+100):
     TotalScore.append([])
     for j in range(201) :
         TotalScore[i].append([])
         for k in range(41) :
            TotalScore[i][j].append(Score(500))

  for i in range(len(files)+100):
    finialSortScore.append([])
  frameSmooth = []
  for i in range(len(files)+100):
    frameSmooth.append([])

  
  for f in files:
     ScorePath = open(TotalScorePath + f, 'r')
     #print(f)
     for lines in ScorePath:
         arr= lines.split(',')
         temp = Score(float(arr[3]))
         #temp.fileName = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].fileName
         #print(int(arr[0]),int(arr[1])+100,int(arr[2])+20) 
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20] = temp
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].HogScore = float(arr[6])
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].SegScore = float(arr[4])
  
  files = listdir(fileNamePath)
  for f in files:
    fileNameData =  open(fileNamePath + f, 'r')
    for lines in fileNameData:
        arr= lines.split('_')
        TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].x = float(arr[3])
        TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].y = float(arr[4])
        TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = float(arr[5])

  for i in range(35): 
    print(i)
    TotalScore,finialSortScore = method1(TotalScore,finialSortScore)
  """
  for i in range(len(finialSortScore)-1):
      print('--------------------------')
      print(i)
      for j in range(0,100):
        print(finialSortScore[i][j].offset-100)
  """

  #計算smooth
  for i in range(1,38):
      for j in range(35):
        for o in range(35):
            for k in range(35):
                lat =TotalScore[i][finialSortScore[i][j].offset][finialSortScore[i][j].heading].x
                lon = TotalScore[i][finialSortScore[i][j].offset][finialSortScore[i][j].heading].y
                nextLat = TotalScore[i+1][finialSortScore[i+1][o].offset][finialSortScore[i+1][o].heading].x
                nextLon = TotalScore[i+1][finialSortScore[i+1][o].offset][finialSortScore[i+1][o].heading].y
                preLat = TotalScore[i-1][finialSortScore[i-1][k].offset][finialSortScore[i-1][k].heading].x
                preLon = TotalScore[i-1][finialSortScore[i-1][k].offset][finialSortScore[i-1][k].heading].y
                dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
                dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
                heading = abs(TotalScore[i+1][finialSortScore[i+1][o].offset][finialSortScore[i+1][o].heading].heading - TotalScore[i][finialSortScore[i][j].offset][finialSortScore[i][j].heading].heading)
                heading += abs(TotalScore[i][finialSortScore[i][j].offset][finialSortScore[i][j].heading].heading - TotalScore[i-1][finialSortScore[i-1][k].offset][finialSortScore[i-1][k].heading].heading)
                output = str(i)+','+str(finialSortScore[i][j].offset-100)+ ','+str(finialSortScore[i][j].heading-20)+', '+str(i+1)+','+str(finialSortScore[i+1][o].offset-100)+','+str(finialSortScore[i+1][o].heading-20)+', '+str(i-1)+','+str(finialSortScore[i-1][k].offset-100)+','+str(finialSortScore[i-1][k].heading-20)+'\n'
                HOGTotal = TotalScore[i][finialSortScore[i][j].offset][finialSortScore[i][j].heading].finialScore +TotalScore[i+1][finialSortScore[i+1][o].offset][finialSortScore[i+1][o].heading].finialScore +TotalScore[i-1][finialSortScore[i-1][k].offset][finialSortScore[i-1][k].heading].finialScore
                output += str(dis1+dis3)+','+str(heading)+','+str(HOGTotal)+'\n\n'
                frameSmooth[i].append(frameDis(finialSortScore[i][j].offset,finialSortScore[i][j].heading,finialSortScore[i+1][o].offset,finialSortScore[i+1][o].heading,finialSortScore[i-1][k].offset,finialSortScore[i-1][k].heading,dis1+dis3,heading,HOGTotal))
                #print(i,finialSortScore[i][j].offset-100,finialSortScore[i][j].heading-20,',',i+1,finialSortScore[i+1][o].offset-100,finialSortScore[i+1][o].heading-20)
                #print(dis1+dis3,heading)
                print(j,o,k)
                #print(TotalScore[i][finialSortScore[i][j].offset][finialSortScore[i][j].heading].finialScore)
                #print(TotalScore[i+1][finialSortScore[i+1][o].offset][finialSortScore[i+1][o].heading].finialScore)
                filenameData= "D:/lab/ITRI/demo/20190426/demo5/method1/smooth2.txt"
                with open(filenameData, 'a') as file:
                    file.write(output)

      minScore = 100000
      offset = -1
      nextoffset = -1
      preOffset = -1
      heading = -1
      nextHeading = -1
      preHeading = -1
      for h in range(0,len(frameSmooth[i])):
        disScore = dealScore(frameSmooth[i][h].dis,300)
        headingScore = dealScore(frameSmooth[i][h].headingDis,15)
        Score = dealScore(frameSmooth[i][h].score,0.7)
        weightOffset = 1
        weightHeading = 2
        weightScore = 1
        frameSmooth[i][h].finialScore = 0.3*(0.5*disScore + 0.5*headingScore) + 0.7*Score
        #print(i,frameSmooth[i][h].offset,frameSmooth[i][h].nextOffset,frameSmooth[i][h].preOffset,disScore,headingScore,Score)
        if frameSmooth[i][h].finialScore < minScore:
            minScore = frameSmooth[i][h].finialScore
            offset = frameSmooth[i][h].offset
            nextoffset = frameSmooth[i][h].nextOffset
            preOffset = frameSmooth[i][h].preOffset
            heading = frameSmooth[i][h].heading
            nextHeading = frameSmooth[i][h].nextHeading
            preHeading = frameSmooth[i][h].preHeading
      filenameData= "D:/lab/ITRI/demo/20190426/demo5/method1/smooth_result2.txt"
      with open(filenameData, 'a') as file:
        string = str(i)+','+str(offset-100)+','+str(nextoffset-100)+','+str(preOffset-100)+','+str(heading-20)+','+str(nextHeading-20)+','+str(preHeading-20)+'\n'
        file.write(string)
      print(i,offset-100,nextoffset-100,preOffset-100,heading-20,nextHeading-20,preHeading-20)
      #print(i,offset-100,heading-20)


