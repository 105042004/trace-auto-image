import cv2
import numpy as np 
from os import listdir
from os.path import isfile, join 
from joblib import Parallel, delayed
import multiprocessing
import threading
import time

def genLineTask_t(args):
  files = args['files']
  mypath = args['mypath']
  testPath = args['testpath']
  for f in files:
    fullpath = join(mypath,f)
    if isfile(fullpath):
      path = mypath+f
      # read image and do some pre-processing
      img = cv2.imread(path)
      hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
      lower_gray=np.array([0,0,46])
      upper_gray=np.array([180,43,220])
      mask = cv2.inRange(hsv, lower_gray, upper_gray)
      # pre-processing step2         
      img = cv2.GaussianBlur(mask,(3,3),0)
      edges = cv2.Canny(img, 100, 150, apertureSize = 3)
      lines = cv2.HoughLines(edges,1,np.pi/180,150) #这里对最后一个参数使用了经验型的值
      result = img.copy()
      
      if  lines is not None:
        for line in lines[0]:
          rho = line[0] #第一个元素是距离rho
          theta= line[1] #第二个元素是角度theta
          #print rho
          #print theta
          if  (theta < (np.pi/4. )) or (theta > (3.*np.pi/4.0)): #垂直直线
            #该直线与第一行的交点
            pt1 = (int(rho/np.cos(theta)),0)
            #该直线与最后一行的焦点
            pt2 = (int((rho-result.shape[0]*np.sin(theta))/np.cos(theta)),result.shape[0])
            #绘制一条白线
            cv2.line( result, pt1, pt2, (255))
            cv2.imwrite(testPath+f+'_line.png', edges)
            #print(fullpath)
          else: #水平直线
            # 该直线与第一列的交点
            pt1 = (0,int(rho/np.sin(theta)))
            #该直线与最后一列的交点
            pt2 = (result.shape[1], int((rho-result.shape[1]*np.cos(theta))/np.sin(theta)))
            #绘制一条直线
            cv2.line(result, pt1, pt2, (255), 1)
            #cv2.imshow('Canny', edges )
            #cv2.imshow('Result', result)
            cv2.imwrite(testPath+f+'_line.png', edges)
            #cv2.waitKey(0)
      else:
          pass
          #print("is no line:"+fullpath)

      # release memory
      del img
      del result
      cv2.destroyAllWindows()


def genLineTask(args):
  f = args['file']

  mypath = args['mypath']
  testPath = args['testpath']
  fullpath = join(mypath,f)
  if isfile(fullpath):
    path = mypath+f
    # read image and do some pre-processing
    img = cv2.imread(path)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lower_gray=np.array([0,0,46])
    upper_gray=np.array([180,43,220])
    mask = cv2.inRange(hsv, lower_gray, upper_gray)
    # pre-processing step2         
    img = cv2.GaussianBlur(mask,(3,3),0)
    edges = cv2.Canny(img, 100, 150, apertureSize = 3)
    lines = cv2.HoughLines(edges,1,np.pi/180,150) #这里对最后一个参数使用了经验型的值
    result = img.copy()
      
    if  lines is not None:
      for line in lines[0]:
        rho = line[0] #第一个元素是距离rho
        theta= line[1] #第二个元素是角度theta
        #print rho
        #print theta
        if  (theta < (np.pi/4. )) or (theta > (3.*np.pi/4.0)): #垂直直线
          #该直线与第一行的交点
          pt1 = (int(rho/np.cos(theta)),0)
          #该直线与最后一行的焦点
          pt2 = (int((rho-result.shape[0]*np.sin(theta))/np.cos(theta)),result.shape[0])
          #绘制一条白线
          cv2.line( result, pt1, pt2, (255))
          cv2.imwrite(testPath+f+'_line.png', edges)
          #print(fullpath)
        else: #水平直线
          # 该直线与第一列的交点
          pt1 = (0,int(rho/np.sin(theta)))
          #该直线与最后一列的交点
          pt2 = (result.shape[1], int((rho-result.shape[1]*np.cos(theta))/np.sin(theta)))
          #绘制一条直线
          cv2.line(result, pt1, pt2, (255), 1)
          #cv2.imshow('Canny', edges )
          #cv2.imshow('Result', result)
          cv2.imwrite(testPath+f+'_line.png', edges)
          #cv2.waitKey(0)
    else:
        pass
        #print("is no line:"+fullpath)
    #cv2.destroyAllWindows()

def lineGenerator(mypath,testPath):
    outputStr = ""
    # 取得所有檔案與子目錄名稱
    files = listdir(mypath)
    #print(mypath)
    #print(testPath)
    # 以迴圈處理
    num = 0
    
    ''' using multiple process
    # Split Task Number
    num_cpus = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(4) # create 12 process
    # prepare task information
    task_args = []
    for f in files:
      task_arg = {}
      task_arg['file'] = f
      task_arg['mypath'] = mypath
      task_arg['testpath'] = testPath
      task_args.append(task_arg)
    pool.map(genLineTask, task_args)
    pool.close()
    pool.join()
    '''

    num_threads =  12 #multiprocessing.cpu_count()
    margin = len(files)/num_threads
    tasks = []
    for n in range(num_threads):
      task_arg = {}
      lower = margin * n
      upper = margin * (n+1) if margin * (n+1) < len(files) else len(files)
      files_split = [files[f] for f in range(int(lower), int(upper))]
      task_arg['files'] = files_split
      task_arg['mypath'] = mypath
      task_arg['testpath'] = testPath
      t = threading.Thread(target=genLineTask_t, args=(task_arg,))
      t.start()
      tasks.append(t)

    for t in tasks:
      t.join()
    '''
    for f in range(0,len(files)):
      # 產生檔案的絕對路徑
      #print(f)
      if True:#isAllOK[num] == 0 :   
          #fullpath 是檔案還是目錄
          fullpath = join(mypath,files[f])
          if isfile(fullpath):
            path = mypath+files[f]
            #print("檔案：", path)
            img = cv2.imread(path)
            hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
            lower_gray=np.array([0,0,46])
            upper_gray=np.array([180,43,220])
            mask = cv2.inRange(hsv, lower_gray, upper_gray)
            #cv2.imshow('mask',mask)
            #cv2.waitKey(0)
            img = cv2.GaussianBlur(mask,(3,3),0)
            edges = cv2.Canny(img, 100, 150, apertureSize = 3)
            lines = cv2.HoughLines(edges,1,np.pi/180,150) #这里对最后一个参数使用了经验型的值
            result = img.copy()
        
            if  lines is not None:
                #print("finish:"+fullpath)
                for line in lines[0]:
                    rho = line[0] #第一个元素是距离rho
                    theta= line[1] #第二个元素是角度theta
                    #print rho
                    #print theta
                if  (theta < (np.pi/4. )) or (theta > (3.*np.pi/4.0)): #垂直直线
                      #该直线与第一行的交点
                    pt1 = (int(rho/np.cos(theta)),0)
                #该直线与最后一行的焦点
                    pt2 = (int((rho-result.shape[0]*np.sin(theta))/np.cos(theta)),result.shape[0])
                #绘制一条白线
                    cv2.line( result, pt1, pt2, (255))
                    cv2.imwrite(testPath+files[f]+'_line.png', edges)
                    #print(fullpath)
                else: #水平直线
                  # 该直线与第一列的交点
                    pt1 = (0,int(rho/np.sin(theta)))
                #该直线与最后一列的交点
                    pt2 = (result.shape[1], int((rho-result.shape[1]*np.cos(theta))/np.sin(theta)))
                #绘制一条直线
                    cv2.line(result, pt1, pt2, (255), 1)
    
                    #cv2.imshow('Canny', edges )
                    #cv2.imshow('Result', result)
                    cv2.imwrite(testPath+files[f]+'_line.png', edges)
                    #cv2.waitKey(0)
            else:
                pass
                #print("is no line:"+fullpath)
      else:
        fullpath = join(mypath, files[f])
        #print("isOK"+fullpath)
      num += 1
    cv2.destroyAllWindows()
    '''
