import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

class positionHeadingData():
 def __init__(self,offset,heading):
  self.offset = offset
  self.heading = heading
  self.score = -1
  self.smoothScore = -1



class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.HogScore = 5000
  self.SegScore = -1
  self.x = 0
  self.y = 0
  self.heading = 0


def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2


if __name__ == '__main__':
    #inputPath = 'F:/straight/temp_0527_circle/groundTruth/result.txt'
    #resultPath = 'F:/straight/temp_0527_circle/method1/result.txt'
    #finialScorePath = 'F:/straight/temp_0527_circle/data/finialScore/'
    #fileNamePath = "F:/straight/temp_0527_circle/data/FileName/"
    inputPath = 'D:/lab/ITRI/demo/20190426/demo10/groundTruth/result.txt'
    resultPath = 'D:/lab/ITRI/demo/20190426/demo10/smoothTerm/result.txt'
    finialScorePath = 'D:/lab/ITRI/demo/20190426/demo10/temp/data/finialScore/'
    fileNamePath = "D:/lab/ITRI/demo/20190426/demo10/temp/data/FileName/"
    startIndex = 1000
    endIndex = -1
    files = listdir(finialScorePath)
    TotalScore = []
    groundTruthResult = []
    finialResult = []
    for i in range(len(files)+420):
        TotalScore.append([])
        groundTruthResult.append(positionHeadingData(-1,-1))
        finialResult.append(positionHeadingData(-1,-1))
        for j in range(201) :
            TotalScore[i].append([])
            for k in range(41) :
                TotalScore[i][j].append(Score(500))

    for f in files:
     ScorePath = open(finialScorePath + f, 'r')
     #print(f)
     for lines in ScorePath:
         arr= lines.split(',')
         temp = Score(float(arr[3]))
         #temp.fileName = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].fileName
         #print(int(arr[0]),int(arr[1])+100,int(arr[2])+20) 
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20] = temp
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].HogScore = float(arr[6])
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].SegScore = float(arr[4])
         if int(arr[0]) < startIndex:
             startIndex = int(arr[0])
         
         if int(arr[0]) > endIndex:
             endIndex = int(arr[0])
         
    
    files = listdir(fileNamePath)
    for f in files:
        fileNameData =  open(fileNamePath + f, 'r')
        for lines in fileNameData:
            #arr= lines.split('_')
            arr3 = lines.split('/')
            arr = arr3[5].split('_')
            #print(lines)
            TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].x = float(arr[3])
            TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].y = float(arr[4])
            TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = float(arr[5])
            #print(TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].x)
    
    
    totalScoreSum = 0
    ScorePath = open(inputPath, 'r')
    for lines in ScorePath:
         arr= lines.split(' ')
         if int(arr[0]) == 6:
            groundTruthResult[int(arr[0])].offset = 87
            groundTruthResult[int(arr[0])].heading = 7
            groundTruthResult[int(arr[0])].score = 0.187878
         elif int(arr[0]) == 7:
            groundTruthResult[int(arr[0])].offset = 87
            groundTruthResult[int(arr[0])].heading = 7
            groundTruthResult[int(arr[0])].score = 0.18831187
         else:
            groundTruthResult[int(arr[0])].offset = int(arr[1])+100
            groundTruthResult[int(arr[0])].heading = int(arr[2])+20
            groundTruthResult[int(arr[0])].score = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].score
         

    for i in range(6,endIndex-1):
        lat =TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].x
        lon = TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].y
        nextLat = TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].x
        nextLon = TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].y
        preLat = TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].x
        preLon = TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].y
        dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
        dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
        heading = abs(TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].heading - TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].heading)
        heading += abs(TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].heading - TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].heading)
        scoreTotal = TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].score +TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].score +TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].score
        #print(TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].score,TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].score,TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].score)
        """if(TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].score>10):
            print(i,groundTruthResult[i].offset-100,groundTruthResult[i].heading-20)
        if(TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].score>10):
            print(i+1,groundTruthResult[i+1].offset-100,groundTruthResult[i+1].heading-20)    
        if(TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].score>10):
            print(i-1,groundTruthResult[i-1].offset-100,groundTruthResult[i-1].heading-20)
        """
        disScore = dealScore(dis1+dis3,300)
        headingScore = dealScore(heading,100)
        Score = dealScore(scoreTotal,0.7)
        finialSmoothScore = 0.3*(0.5*disScore + 0.5*headingScore) + 0.7*Score
        totalScoreSum += finialSmoothScore
        print(i,dis1+dis3,heading,scoreTotal,finialSmoothScore)


    for i in range(6,0,-1):
        lat =TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].x
        lon = TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].y
        nextLat = TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].x
        nextLon = TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].y
        preLat = TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].x
        preLon = TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].y
        dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
        dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
        heading = abs(TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].heading - TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].heading)
        heading += abs(TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].heading - TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].heading)
        scoreTotal = TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].score +TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].score +TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].score
        #print(TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].score,TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].score,TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].score)
        """if(TotalScore[i][groundTruthResult[i].offset][groundTruthResult[i].heading].score>10):
            print(i,groundTruthResult[i].offset-100,groundTruthResult[i].heading-20)
        if(TotalScore[i+1][groundTruthResult[i+1].offset][groundTruthResult[i+1].heading].score>10):
            print(i+1,groundTruthResult[i+1].offset-100,groundTruthResult[i+1].heading-20)    
        if(TotalScore[i-1][groundTruthResult[i-1].offset][groundTruthResult[i-1].heading].score>10):
            print(i-1,groundTruthResult[i-1].offset-100,groundTruthResult[i-1].heading-20)
        """
        disScore = dealScore(dis1+dis3,300)
        headingScore = dealScore(heading,100)
        Score = dealScore(scoreTotal,0.7)
        finialSmoothScore = 0.3*(0.5*disScore + 0.5*headingScore) + 0.7*Score
        totalScoreSum += finialSmoothScore
        print(i,dis1+dis3,heading,scoreTotal,finialSmoothScore)


    
    totalScoreSum2 = 0
    ScorePath = open(resultPath, 'r')
    for lines in ScorePath:
         arr= lines.split(' ')
         finialResult[int(arr[0])].offset = int(arr[1])+100
         finialResult[int(arr[0])].heading = int(arr[2])+20
         finialResult[int(arr[0])].score = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].score
         


    for i in range(6,endIndex-1):
        lat =TotalScore[i][finialResult[i].offset][finialResult[i].heading].x
        lon = TotalScore[i][finialResult[i].offset][finialResult[i].heading].y
        nextLat = TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].x
        nextLon = TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].y
        preLat = TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].x
        preLon = TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].y
        dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
        dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
        heading = abs(TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].heading - TotalScore[i][finialResult[i].offset][finialResult[i].heading].heading)
        heading += abs(TotalScore[i][finialResult[i].offset][finialResult[i].heading].heading - TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].heading)
        scoreTotal = TotalScore[i][finialResult[i].offset][finialResult[i].heading].score +TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].score +TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].score
        if(TotalScore[i][finialResult[i].offset][finialResult[i].heading].score>10):
            print(i,finialResult[i].offset-100,finialResult[i].heading-20)
        if(TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].score>10):
            print(i+1,finialResult[i+1].offset-100,finialResult[i+1].heading-20)    
        if(TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].score>10):
            print(i-1,finialResult[i-1].offset-100,finialResult[i-1].heading-20)
        
        disScore = dealScore(dis1+dis3,300)
        headingScore = dealScore(heading,100)
        Score = dealScore(scoreTotal,0.7)
        finialSmoothScore = 0.3*(0.5*disScore + 0.5*headingScore) + 0.7*Score
        totalScoreSum2 += finialSmoothScore
        print(i,dis1+dis3,heading,scoreTotal,finialSmoothScore)


    for i in range(6,0,-1):
        lat =TotalScore[i][finialResult[i].offset][finialResult[i].heading].x
        lon = TotalScore[i][finialResult[i].offset][finialResult[i].heading].y
        nextLat = TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].x
        nextLon = TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].y
        preLat = TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].x
        preLon = TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].y
        dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
        dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
        heading = abs(TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].heading - TotalScore[i][finialResult[i].offset][finialResult[i].heading].heading)
        heading += abs(TotalScore[i][finialResult[i].offset][finialResult[i].heading].heading - TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].heading)
        scoreTotal = TotalScore[i][finialResult[i].offset][finialResult[i].heading].score +TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].score +TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].score
        if(TotalScore[i][finialResult[i].offset][finialResult[i].heading].score>10):
            print(i,finialResult[i].offset-100,finialResult[i].heading-20)
        if(TotalScore[i+1][finialResult[i+1].offset][finialResult[i+1].heading].score>10):
            print(i+1,finialResult[i+1].offset-100,finialResult[i+1].heading-20)    
        if(TotalScore[i-1][finialResult[i-1].offset][finialResult[i-1].heading].score>10):
            print(i-1,finialResult[i-1].offset-100,finialResult[i-1].heading-20)
        
        disScore = dealScore(dis1+dis3,300)
        headingScore = dealScore(heading,100)
        Score = dealScore(scoreTotal,0.7)
        finialSmoothScore = 0.3*(0.5*disScore + 0.5*headingScore) + 0.7*Score
        totalScoreSum2 += finialSmoothScore
        print(i,dis1+dis3,heading,scoreTotal,finialSmoothScore)
    
    print(totalScoreSum,totalScoreSum2)


 

         