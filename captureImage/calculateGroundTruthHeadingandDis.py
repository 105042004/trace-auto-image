import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

class positionHeadingData():
 def __init__(self,x,y,heading):
  self.x = x
  self.y = y
  self.heading = heading
  self.score = -1
  self.smoothScore = -1



class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.HogScore = 5000
  self.SegScore = -1
  self.x = 0
  self.y = 0
  self.heading = 0


def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2


if __name__ == '__main__':
    #inputPath = 'F:/straight/temp_0527_circle/groundTruth/result.txt'
    #resultPath = 'F:/straight/temp_0527_circle/method1/result.txt'
    #finialScorePath = 'F:/straight/temp_0527_circle/data/finialScore/'
    #fileNamePath = "F:/straight/temp_0527_circle/data/FileName/"
    outPath = 'D:/lab/ITRI/demo/20190426/demo1/'
    inputTotalPath = outPath + 'groundTruth/'
    resultTotalPath = outPath+'3_8_smoothTerm/'
    inputPath = inputTotalPath + 'position_LS.txt'
    #resultPath = resultTotalPath+'position_interpolateFitting.txt'
    resultPath = resultTotalPath+'position_LS.txt'
    #inputHeadingPath = inputTotalPath + 'position_lineFitting_heading.txt'
    #resultHeadingPath = resultTotalPath +  'position_lineFitting_heading.txt'
    finialScorePath = outPath + 'temp/data/finialScore/'
    fileNamePath = outPath + "temp/data/FileName/"
    startIndex = 1000
    endIndex = -1
    files = listdir(finialScorePath)
    TotalScore = []
    groundTruthResult = []
    finialResult = []
    for i in range(len(files)+420):
        TotalScore.append([])
        groundTruthResult.append(positionHeadingData(-1,-1,-1))
        finialResult.append(positionHeadingData(-1,-1,-1))
        for j in range(201) :
            TotalScore[i].append([])
            for k in range(41) :
                TotalScore[i][j].append(Score(500))
    
    
    totalScoreSum = 0
    totalSumNumber = 0
    ScorePath = open(inputPath, 'r')
    index = 0
    for lines in ScorePath:
         arr= lines.split(',')
         groundTruthResult[index].x = float(arr[0])
         groundTruthResult[index].y = float(arr[1])
         groundTruthResult[index].heading= float(arr[2])
         if index < startIndex:
             startIndex = index
         
         if index > endIndex:
             endIndex = index
         #groundTruthResult[index].score = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].score
         index = index+1
         totalSumNumber = index

    """HeadingPath = open(inputHeadingPath, 'r')
    index = 0
    for lines in HeadingPath:
         groundTruthResult[index].heading = float(lines)
         index = index+1
    """

    totalSumNumber2 = 0
    totalScoreSum2 = 0
    ScorePath = open(resultPath, 'r')
    index = 0
    startIndex = 1000
    endIndex = -1
    
    for lines in ScorePath:
         arr= lines.split(',')
         finialResult[index].x = float(arr[0])
         finialResult[index].y = float(arr[1])
         finialResult[index].heading = float(arr[2])
         if index < startIndex:
             startIndex = index
         
         if index > endIndex:
             endIndex = index
         #finialResult[index].score = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].score
         index = index+1

    disTotal = 0
    headingTotal = 0
    
    #HeadingPath = open(resultHeadingPath, 'r')
    #index = 0
    #for lines in HeadingPath:
    #     finialResult[index].heading = float(lines)
    #     index = index + 1
    
    totalCount = 0
    for i in range(startIndex+1,endIndex-1):
        if groundTruthResult[i].x != -1 :
            totalCount = totalCount + 1
            lat = groundTruthResult[i].x
            lon = groundTruthResult[i].y
            nextLat = finialResult[i].x
            nextLon = finialResult[i].y
            
            dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
            #if groundTruthResult[i].heading < 0:
            #    groundTruthResult[i].heading = 360 + groundTruthResult[i].heading
            heading = abs(finialResult[i].heading -groundTruthResult[i].heading)
            disScore = dealScore(dis1,500)
            headingScore = dealScore(heading,20)
            finialSmoothScore = 0.5*disScore + 0.5*headingScore
            totalScoreSum += finialSmoothScore
            headingTotal += heading
            disTotal += dis1
            print(i,heading,dis1,disScore)
            #print(i,finialSmoothScore,heading,headingScore,dis1,disScore)

    print(headingTotal/totalCount)
    print(disTotal/totalCount)
    #print(totalScoreSum/totalCount)
