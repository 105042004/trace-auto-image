import os
import cv2
import numpy as np
from os import listdir
from os.path import isfile, isdir, join


segImagePath = 'D:/0826/AutoData_demo/20190912_turn_right/Seg/'#The last '/' must exist .
laneImagePath = 'D:/0826/AutoData_demo/20190912_turn_right/lane/'
lineImagePath = 'D:/0826/AutoData_demo/20190912_turn_right/line/'
#test = 'D:/lab/ITRI/demo/20190426/image/curve/edge/'
savePath = 'D:/0826/AutoData_demo/20190912_turn_right/finialLine/'

if __name__ == '__main__':
 
    color = [
     ([135,135,135], [145,145,145])
    ]
    files = listdir(lineImagePath)
    for i in range(0,len(files)):
        print(i)
        lineImage  = cv2.imread(lineImagePath + files[i],cv2.IMREAD_GRAYSCALE )
        width, height = lineImage.shape[:2]
        arr = files[i].split('.')
        segImage = cv2.imread(segImagePath + arr[0]+'.png' )
        #testImage = cv2.imread(lineImagePath + arr[0]+'.png',cv2.IMREAD_GRAYSCALE )

        for (lower, upper) in color:
            # 创建NumPy数组
            lower = np.array(lower, dtype = "uint8")#颜色下限
            upper = np.array(upper, dtype = "uint8")#颜色上限
        
            # 根据阈值找到对应颜色
            segMask = cv2.inRange(segImage, lower, upper)
            segOutput = cv2.bitwise_and(segImage, segImage, mask = segMask)
        gray = cv2.cvtColor(segOutput, cv2.COLOR_BGR2GRAY)
        contours, hierarchy = cv2.findContours(gray.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        #create a black use numpy,size is:512*512
        segResult = np.zeros((width,height,3), np.uint8)   
        #fill the image with white
        segResult.fill(0)
        cv2.drawContours(segResult,contours,-1,(255,255,255),30)


        #cv2.imshow("images", np.hstack([segResult, segOutput]))
        #cv2.waitKey(0)



       
        laneImage1 = cv2.imread(laneImagePath +''+str(arr[0])+'_'+'1_avg.png',cv2.IMREAD_GRAYSCALE)
        laneImage1 = cv2.resize(laneImage1,(height,width), interpolation=cv2.INTER_CUBIC)
        laneImage2 = cv2.imread(laneImagePath +''+str(arr[0])+'_'+'2_avg.png',cv2.IMREAD_GRAYSCALE)
        laneImage2 = cv2.resize(laneImage2,(height,width), interpolation=cv2.INTER_CUBIC)
        laneImage3 = cv2.imread(laneImagePath +''+str(arr[0])+'_'+'3_avg.png',cv2.IMREAD_GRAYSCALE)
        laneImage3 = cv2.resize(laneImage3,(height,width), interpolation=cv2.INTER_CUBIC)
        laneImage4 = cv2.imread(laneImagePath +''+str(arr[0])+'_'+'4_avg.png',cv2.IMREAD_GRAYSCALE)
        laneImage4 = cv2.resize(laneImage4,(height,width), interpolation=cv2.INTER_CUBIC)

        ret , thresh1 = cv2.threshold(laneImage1,50,255,cv2.THRESH_BINARY)
        ret , thresh2 = cv2.threshold(laneImage2,50,255,cv2.THRESH_BINARY)
        ret , thresh3 = cv2.threshold(laneImage3,50,255,cv2.THRESH_BINARY)
        ret , thresh4 = cv2.threshold(laneImage4,50,255,cv2.THRESH_BINARY)
        result = cv2.bitwise_or(thresh1,thresh2)
        result = cv2.bitwise_or(result,thresh3)
        result = cv2.bitwise_or(result,thresh4)
        masked = cv2.bitwise_and(lineImage, lineImage, mask=result)
        segResult = cv2.cvtColor(segResult,cv2.COLOR_BGR2GRAY)
        segMasked = cv2.bitwise_and(lineImage, lineImage, mask=segResult)

        mg_mix = cv2.addWeighted(segMasked, 0.6, masked, 0.4, 0)
        cv2.imwrite(savePath+str(arr[0])+'.png',mg_mix )
        #cv2.imshow(str(arr[0]),np.hstack([mg_mix,testImage]))
        #cv2.waitKey(0)




"""
        cv2.imshow(str(arr[0]),masked)
        cv2.imshow("tt",lineImage)
        cv2.waitKey(0)
   


        cv2.imshow("tt",laneImage1)
        cv2.imshow("tt1",laneImage2)
        cv2.imshow("tt2",laneImage3)
        cv2.imshow("tt3",laneImage4)
        cv2.waitKey(0)
        """
