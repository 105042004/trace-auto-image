import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize
from scipy import interpolate

PositionPath = 'D:/AutoImage/data/model_filled_afterUnreal/temp_after/'


ResultData = open(PositionPath + "position.txt", 'r')
x_coord = []
y = []
heading = []
tempHeading = []
output = ""
count = 0
for lines in ResultData:
    if count>=0:
        arr = lines.split(',')
        x_coord.append(float(arr[0]) )
        y.append(float(arr[1]) )
        heading.append(count)
        tempHeading.append(float(arr[2]))
        #print(float(arr[0]))
        #print(float(arr[1]))
        count = count+1

#處理heading
isDeal = False
for i in range(0,len(tempHeading)-1):
	if tempHeading[i]*tempHeading[i+1] < 0 and abs(tempHeading[i] - tempHeading[i+1]) >50:
		isDeal = True
if isDeal == True:
	for i in range(0,len(tempHeading)):
		if tempHeading[i]<0:
			tempHeading[i]= 360+tempHeading[i]


"""
minX =x_coord[0]
maxX = x_coord[len(x_coord)-1]
for i in range(0,len(x_coord)):
    x_coord[i] = minX + i*(maxX - minX)/len(x_coord)
    #print(x_coord[i])
"""
x_ori = []
y_ori = []
tempHeading_ori = []
for i in range(0,len(x_coord)):
        x_ori.append(x_coord[i])
        y_ori.append(y[i])
        tempHeading_ori.append(tempHeading[i])


def laplacian_operator(data):
    """
    apply laplacian operator on data
    """

    lap = []
    lap.append(0.0)
    for index in range(1, len(data) - 1):
        lap.append( (data[index + 1] + data[index - 1]) / 2.0 - data[index] )
        #print(data[index + 1],data[index - 1],data[index], (data[index + 1] + data[index - 1]) / 2.0 - data[index])
    lap.append(0.0)
    return lap


def laplacian_smooth(data, alpha=0.5, iterations=3):
    """
    apply laplacian smoothing on data
    """

    for iteration in range(iterations):
        lap = laplacian_operator(data)
        for index in range(len(data)):
            #print('1: ',data[index])
            data[index] += alpha * lap[index]
            #print('2: ',data[index],alpha,lap[index])
    return data


x_coord =laplacian_smooth(x_coord, 0.5, 60)
y = laplacian_smooth(y, 0.5, 60)

output = ''
#print(len(heading),len(y_result))
for i in range(0,len(x_coord)):
    print(x_coord[i],x_ori[i])
    print(y[i],y_ori[i])
    #output += str()+','+str(y_result[i])+',\n'
    #print(heading[i],y_result[i])

plt.plot(x_ori, y_ori, 'b-', x_coord,y, 'r:')
plt.show()

heading = laplacian_smooth(heading, 0.5, 100)
tempHeading = laplacian_smooth(tempHeading, 0.5, 100)

for i in range(0,len(x_coord)):
    print(tempHeading_ori[i],tempHeading[i])
    output += str(x_coord[i])+','+str(y[i])+','+str(tempHeading[i])+',\n'
    #print(heading[i],y_result[i])

plt.plot(heading, tempHeading_ori, 'b-', heading,tempHeading, 'r:')
plt.show()

filenameData= PositionPath +"position_Fixed_LS.txt"
with open(filenameData, 'w') as file:
   file.write(output)



