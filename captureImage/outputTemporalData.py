import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

outPath = 'D:/AutoImage/data/model_filled_afterUnreal/temp_after/'
fileName = 'smoothTermResult_3_8.txt'
outputData = "result.txt"


class data():
 def __init__(self,index,offsetIndex,headingIndex):
  self.index = index
  self.offsetIndex = offsetIndex
  self.headingIndex = headingIndex

class score():
  def __init__(self,index,score):
    self.score = score
    self.index = index
TotalRound = []
TotalScore = []
finialScore = []
output = ''

if __name__ == '__main__':
    DataPath = open(outPath+fileName, 'r')
    roundN = -1
    for lines in DataPath:
      arr = lines.split(' ')
      if str(arr[0]) == "round":
        TotalRound.append([])
        roundN = int(arr[1]) #arr[1] = frame number
      else:
        arr1 = lines.split(',')
        if len(arr1) == 4:
           TotalRound[roundN].append(data((arr1[0]),arr1[1],arr1[2]))
        if len(arr1) == 3:
          TotalScore.append(score(len(TotalScore),float(arr1[1])))

    minN = 999999999999
    index = -1
    for i in range(0,len(TotalScore)):
      if TotalScore[i].score < minN:
        minN = TotalScore[i].score
        index = i
        
    for i in range(0,int(TotalRound[index][len(TotalRound[index])-1].index)):
      isValuable = False
      for j in range(0,len(TotalRound[index])):
        if int(TotalRound[index][j].index) == i:
          finialScore.append(TotalRound[index][j])
          isValuable = True
      if isValuable == False:
        finialScore.append(data(i,-1000,-1000))
        
    for i in range(0,len(finialScore)):
      if finialScore[i].offsetIndex == -1000:
        for j in range(0,len(finialScore)):
          if finialScore[j].offsetIndex != -1000:
            finialScore[i].offsetIndex =finialScore[j].offsetIndex
            finialScore[i].headingIndex =finialScore[j].headingIndex
            output += str(finialScore[i].index)+' '+str(finialScore[i].offsetIndex)+' '+str(finialScore[i].headingIndex)+'\n'
            print(finialScore[i].index,finialScore[i].offsetIndex,finialScore[i].headingIndex)
            break
      else:
        output += str(finialScore[i].index)+' '+str(finialScore[i].offsetIndex)+' '+str(finialScore[i].headingIndex)+'\n'
        print(finialScore[i].index,finialScore[i].offsetIndex,finialScore[i].headingIndex)

    filenameData= outPath +outputData
    with open(filenameData, 'w') as file:
       file.write(output)

