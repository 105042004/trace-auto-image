import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealHOGScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

class position():
 def __init__(self,lat,lon,heading):
  self.lat = lat
  self.lon = lon
  self.heading = heading

if __name__ == '__main__':
  #PositionPath = 'F:/straight/temp_0527_circle/'
  #PositionPath = 'F:/straight/temp_0523_curve/'
  PositionPath = 'F:/straight/temp_straight_0518/'
  fileList = ['method1/','method2/','method5/','method10/','method13/']
  maxDis = 0
  maxHeading = 0
  totalDisList = []
  totalHeadingList = []
  for i in range(0,len(fileList)):
      print(fileList[i])
      allPositionList = []
      totalDis = 0
      totalHeading = 0
      files = listdir(PositionPath+fileList[i])
      PositionData = open(PositionPath+fileList[i] +'position.txt', 'r')
      for lines in PositionData:
          arr = lines.split(',')
          allPositionList.append(position(float(arr[0]),float(arr[1]),float(arr[2]) ))
          #print(allPositionList[ len(allPositionList)-1 ].lat,allPositionList[len(allPositionList)-1].lon,allPositionList[len(allPositionList)-1].heading)
      for i in range(0,len(allPositionList)-1):
        vector1 = np.array([allPositionList[i].lat,allPositionList[i].lon,0])
        vector2 = np.array([allPositionList[i+1].lat,allPositionList[i+1].lon,0])
        op1=np.sqrt(np.sum(np.square(vector1-vector2)))
        op2=np.linalg.norm(vector1-vector2)
        if(maxDis<op1):
          maxDis = op1
        if(maxHeading < abs(allPositionList[i+1].heading -  allPositionList[i].heading)):
          maxHeading = abs(allPositionList[i+1].heading -  allPositionList[i].heading)
        totalDis += op1
        totalHeading += abs(allPositionList[i+1].heading -  allPositionList[i].heading)
        #print(i)
        #print(op1)
        #print('----------------')
      print(totalDis)
      print(totalHeading)
      totalDisList.append(totalDis)
      totalHeadingList.append(totalHeading)
  for i in range(0,len(totalDisList)):
    finialScore = 0.5*dealHOGScore(totalHeadingList[i],totalHeadingList[totalHeadingList.index(max(totalHeadingList))]) + 0.5*dealHOGScore(totalDisList[i],totalDisList[totalDisList.index(max(totalDisList))])
    print(finialScore)
finial = 0.5*dealHOGScore(253,262) + 0.5*dealHOGScore(11936,12014)
print(finial)


