import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
import os
from os import listdir
from os.path import isfile, isdir, join

class positionHeadingData():
 def __init__(self,x,y,heading):
  self.x = x
  self.y = y
  self.heading = heading
  self.offsetIndex = 0
  self.headingIndex = 0
  self.score = -1
  self.smoothScore = -1



class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.HogScore = 5000
  self.SegScore = -1
  self.x = 0
  self.y = 0
  self.heading = 0


def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2


if __name__ == '__main__':
    outPath = 'D:/lab/ITRI/demo/20190426/demo4/'
    inputTotalPath = outPath + 'groundTruth/'
    resultTotalPath = outPath
    inputPath = inputTotalPath + 'position_LS.txt'

    finialScorePath = outPath + 'temp/data/finialScore/'
    fileNamePath = outPath + "temp/data/FileName/"

    startIndex = 1000
    endIndex = -1
    files = listdir(finialScorePath)
    TotalScore = []
    groundTruthResult = []
    finialResult = []
    for j in range(20):
      finialResult.append([])
      for i in range(len(files)+420):
        finialResult[j].append(positionHeadingData(-1,-1,-1))

    
    for i in range(len(files)+420):
        TotalScore.append([])
        groundTruthResult.append(positionHeadingData(-1,-1,-1))
        for j in range(201) :
            TotalScore[i].append([])
            for k in range(41) :
                TotalScore[i][j].append(Score(500))
    
    
    totalScoreSum = 0
    totalSumNumber = 0
    ScorePath = open(inputPath, 'r')
    count = 0 
    for lines in ScorePath:
         arr= lines.split(',')
         groundTruthResult[count].x = float(arr[0])
         groundTruthResult[count].y = float(arr[1])
         groundTruthResult[count].heading = float(arr[2])
         count = count+1

            
         #groundTruthResult[index].score = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].score
         
    
    #讀取權重結果
    for j in range(1,10):
        for k in range(5,10):
            resultPath = resultTotalPath+'tempsmoothTermResult_'+str(j)+'_'+str(k)+'.txt'
            if os.path.isfile(resultPath):
                #print("檔案存在。")
                resultScore =  open(resultPath, 'r')
                roundNumber = 0
                roundScore = np.full(20,1000.0)
                fScire = 0
                for result in resultScore:
                    arr = result.split(',')
                    if len(arr) < 2:
                        arr1 = result.split(' ')
                        roundNumber = int(arr1[1])
                        fScire = 0
                    elif len(arr) <= 3:
                        roundScore[fScire] = float(arr[1])
                        fScire = fScire +1
                    elif len(arr) >3:
                        #print(roundNumber,arr,len(finialResult[0]))
                        finialResult[roundNumber][int(arr[0])].offsetIndex = int(arr[1])
                        finialResult[roundNumber][int(arr[0])].headingIndex = int(arr[2])
                        fileNameData = open(fileNamePath+str(arr[0])+'.txt', 'r')
                        fScire = 0
                        if startIndex > int(arr[0]):
                            startIndex = int(arr[0])
                        if endIndex < int(arr[0]):
                            endIndex = int(arr[0])
                        for filename in fileNameData:
                            arr1 = filename.split('_')
                            if int(arr1[1]) == int(arr[1]) and int(arr1[2]) == int(arr[2]):
                                finialResult[roundNumber][int(arr[0])].x = float(arr1[3])
                                finialResult[roundNumber][int(arr[0])].y = float(arr1[4])
                                finialResult[roundNumber][int(arr[0])].heading = float(arr1[5])
                index_of_maximum = np.where(roundScore == np.min(roundScore))
                roundIndex = index_of_maximum[0][0]
                #print(roundScore)
                #print(index_of_maximum[0][0])
                #for ss in range(len(finialResult[int(roundIndex)])):
                #    if ss <= endIndex:
                #        print( ss,finialResult[roundIndex][ss].offsetIndex, finialResult[roundIndex][ss].headingIndex )

                #計算分數
                totalCount = 0
                headingTotal = 0
                disTotal = 0
                for i in range(startIndex+1,endIndex-1):
                    if groundTruthResult[i].x != -1 and finialResult[roundIndex][i].x != -1:
                        
                        totalCount = totalCount + 1
                        lat = groundTruthResult[i].x
                        lon = groundTruthResult[i].y
                        nextLat = finialResult[roundIndex][i].x
                        nextLon = finialResult[roundIndex][i].y
                        
                        dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
                        if groundTruthResult[i].heading < 0:
                            groundTruthResult[i].heading = 360 + groundTruthResult[i].heading
                        if finialResult[roundIndex][i].heading < 0:
                            finialResult[roundIndex][i].heading = 360 + finialResult[roundIndex][i].heading
                        heading = abs(finialResult[roundIndex][i].heading -groundTruthResult[i].heading)
                        disScore = dealScore(dis1,500)
                        headingScore = dealScore(heading,20)
                        finialSmoothScore = 0.5*disScore + 0.5*headingScore
                        totalScoreSum += finialSmoothScore
                        headingTotal += heading
                        disTotal += dis1
                        print(i,dis1,finialResult[roundNumber][i].offsetIndex,lat,nextLat)
                        #print(i,heading,dis1,disScore)
                        #print(i,finialSmoothScore,heading,headingScore,dis1,disScore)
                print('DataTerm ',j,' smoothTerm ',k,'  heading:',headingTotal/totalCount,' Dis:',disTotal/totalCount)
                #print(headingTotal/totalCount)
                #print(disTotal/totalCount)
            





