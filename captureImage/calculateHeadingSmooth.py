import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
import os
import json
import math
from os import listdir
from os.path import isfile, isdir, join

fileNamePath = 'F:/straight/temp_0527_circle/method10/'

def angle(v1, v2):
    dx1 = v1[2] - v1[0]
    dy1 = v1[3] - v1[1]
    dx2 = v2[2] - v2[0]
    dy2 = v2[3] - v2[1]
    angle1 = math.atan2(dy1, dx1)
    angle1 = float(angle1 * 180/math.pi)
    #print(angle1)
    angle2 = math.atan2(dy2, dx2)
    angle2 = float(angle2 * 180/math.pi)
    #print(angle2)
    if angle1*angle2 >= 0:
        included_angle = abs(angle1-angle2)
    else:
        included_angle = abs(angle1) + abs(angle2)
        if included_angle > 180:
            included_angle = 360 - included_angle
    return angle1

def readTxtFile():
    ResultData = open(fileNamePath + "position_lineFitting.txt", 'r')
    count = 0
    location = []
    rotate = []
    for lines in ResultData:
        arr = lines.split(',')
        location.append(float(arr[0]))
        location.append(float(arr[1]))

    for k in range(0,int(len(location)/2)):
        loc = dict(x = location[2*k], y = location[2*k+1], z = 186.0)
        nextLoc = dict(x = location[2*(k+1)], y = location[2*(k+1)+1], z = 186.0)
        vector1 = (float(loc['x']*100),float(loc['y']*100),0.0)
        vector2 = (float(nextLoc['x']*100),float(nextLoc['y']*100),0.0)
        rot1 = angle( [vector1[0],vector1[1],vector2[0],vector2[1] ] , [0,0,1,0])
        print(rot1)

if __name__ == '__main__':
    readTxtFile()