import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

outPath = 'D:/lab/ITRI/demo/20190426/demo8/'
filePath = outPath + '3_8_smoothTerm/'
fileName = 'fixed_data_result.txt'
finialScorePath = outPath + 'temp/data/finialScore/'
positionData = outPath + "temp/data/FileName/"

class positionHeadingData():
 def __init__(self,offsetIndex,headingIndex):
  self.offsetIndex = offsetIndex
  self.headingIndex = headingIndex
  self.weight = 0
  self.x = -1000
  self.y = -1000
  self.heading = -1000
  self.nextOffsetIndex = offsetIndex
  self.nextHeadingIndex = headingIndex
  self.nextWeight = 0
  self.nextX = -1000
  self.nextY = -1000
  self.nextHeading = -1000

def dealScore(x,maxNumber):
    return (x/maxNumber)

if __name__ == '__main__':

    files = listdir(finialScorePath)
    dataList = []
    countNumber = []
    countNumber.append(0)
    for i in range(len(files)+420):
        dataList.append(positionHeadingData(-1000,-1000))
    
    DataPath = open(filePath+fileName, 'r')
    count = 0
    for lines in DataPath:
        arr = lines.split(' ')
        if dataList[int(arr[0])].offsetIndex == -1000:
            dataList[int(arr[0])].offsetIndex = int(arr[1])
            dataList[int(arr[0])].headingIndex = int(arr[2])
            dataList[int(arr[0])].weight = count
            fileNameData = open(positionData+str(arr[0])+'.txt', 'r')
            for filename in fileNameData:
                arr1 = filename.split('_')
                if int(arr1[1]) == int(arr[1]) and int(arr1[2]) == int(arr[2]):
                    dataList[int(arr[0])].x = float(arr1[3])
                    dataList[int(arr[0])].y = float(arr1[4])
                    dataList[int(arr[0])].heading = float(arr1[5])
            count = count +1
            if countNumber[len(countNumber)-1]  == count -2 :
                countNumber[len(countNumber)-1] = count -1
            else:
                countNumber.append(0)
             
            if(arr[3] == '\n'):
                dataList[int(arr[0])].weight = -1
                count = 1


        else:
            dataList[int(arr[0])].nextOffsetIndex = int(arr[1])
            dataList[int(arr[0])].nextHeadingIndex = int(arr[2])
            dataList[int(arr[0])].nextWeight = count
            fileNameData = open(positionData+str(arr[0])+'.txt', 'r')
            for filename in fileNameData:
                arr1 = filename.split('_')
                if int(arr1[1]) == int(arr[1]) and int(arr1[2]) == int(arr[2]):
                    dataList[int(arr[0])].nextX = float(arr1[3])
                    dataList[int(arr[0])].nextY = float(arr1[4])
                    dataList[int(arr[0])].nextHeading = float(arr1[5])
            count = count +1 
            if countNumber[len(countNumber)-1]  == count -2 :
                countNumber[len(countNumber)-1] = count -1
            else:
                countNumber.append(0)

            if(arr[3] == '\n'):
                dataList[int(arr[0])].weight = -1
                count = 1
    print(countNumber)
    finialCount = 0
    output= ''
    outputDat = ''
    for i in range(len(dataList)):
        #print(countNumber)
        if dataList[i].offsetIndex != -1000:
            if dataList[i].weight == -1 and dataList[i+1].weight != -1:
                finialCount = dataList[i+1].nextWeight
                print(finialCount)
            if dataList[i].weight != -1 and dataList[i].nextOffsetIndex != -1000:
                print(finialCount)
                disX = dataList[i].x * (1-dealScore(dataList[i].weight,finialCount+1))
                nextDisX = dataList[i].nextX * (1-dealScore(dataList[i].nextWeight,finialCount+1))
                print(dataList[i].x,dataList[i].nextX,disX+nextDisX,(1-dealScore(dataList[i].weight,finialCount+1)),(1-dealScore(dataList[i].nextWeight,finialCount+1)) )
                disY = dataList[i].y * (1-dealScore(dataList[i].weight,finialCount+1))
                nextDisY = dataList[i].nextY * (1-dealScore(dataList[i].nextWeight,finialCount+1))
                print(dataList[i].y,dataList[i].nextY,disY+nextDisY,(1-dealScore(dataList[i].weight,finialCount+1)),(1-dealScore(dataList[i].nextWeight,finialCount+1)) )
                disH = dataList[i].heading * (1-dealScore(dataList[i].weight,finialCount+1))
                nextDisH = dataList[i].nextHeading * (1-dealScore(dataList[i].nextWeight,finialCount+1))
                output += str(disX+nextDisX)+','+str(disY+nextDisY) +',' +str(disH+nextDisH)+'\n'
                outputDat += str(i) + ' '+str(0.43785) + ' '+str(disX+nextDisX)+' '+str(disY+nextDisY)+' '+str(count*100)+'\n'
            else:
                print(dataList[i].x)
                output += str(dataList[i].x)+','+str(dataList[i].y) +',' +str(dataList[i].heading)+'\n'
                outputDat += str(i) + ' '+str(0.43785) + ' '+str(dataList[i].x)+' '+str(dataList[i].y)+' '+str(count*100)+'\n'

            print(i,str(dataList[i].offsetIndex)+','+str(dataList[i].headingIndex)+','+str(dataList[i].weight),str(dataList[i].nextOffsetIndex)+','+str(dataList[i].nextHeadingIndex)+','+str(dataList[i].nextWeight),'\n' )

    filenameData= filePath +"position_fixed.txt"
    with open(filenameData, 'w') as file:
       file.write(output)
    filenameData= filePath +"position_fixed.dat"
    with open(filenameData, 'w') as file:
       file.write(outputDat)