import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

class positionHeadingData():
 def __init__(self,offset,heading):
  self.offset = offset
  self.heading = heading
  self.score = -1
  self.smoothScore = -1



class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.HogScore = 5000
  self.SegScore = -1
  self.x = 0
  self.y = 0
  self.heading = 0

class finialIndex():
  def __init__(self):
    self.index = -1
    self.frontIndex = -1
    self.heading = -1
    self.frontHeading = -1



output = ''

def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2

def calculateSmoothScore(index,value,offset1,offset2,offset3,heading1,heading2,heading3):
    lat =TotalScore[index][offset1][heading1].x
    lon = TotalScore[index][offset1][heading1].y
    nextLat = TotalScore[index+value][offset2][heading2].x
    nextLon = TotalScore[index+value][offset2][heading2].y
    nextnextLat = TotalScore[index+value*2][offset3][heading3].x
    nextnextLon = TotalScore[index+value*2][offset3][heading3].y
    dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
    dis3,dis4 = calculateDis(lat,lon,nextnextLat,nextnextLon)
    heading = abs(TotalScore[index+value][offset2][heading2].heading - TotalScore[index][offset1][heading1].heading)
    heading += abs(TotalScore[index][offset1][heading1].heading - TotalScore[index+value*2][offset3][heading3].heading)
    distance = dis1+dis3
    if TotalScore[index][offset1][heading1].score <5 and TotalScore[index+value][offset2][heading2].score<5 and TotalScore[index+value*2][offset3][heading3].score<5:
       HOGSEGTotalScore = TotalScore[index][offset1][heading1].score +TotalScore[index+value][offset2][heading2].score +TotalScore[index+value*2][offset3][heading3].score
       disScore = dealScore(distance,1000)
       headingScore = dealScore(heading,100)
       Score = dealScore(HOGSEGTotalScore,1.0)
       finialS =  0.3*(0.8*disScore + 0.2*headingScore) + 0.7*Score
       #if index == 41:
       #print(index,index+1,index-1,offset1,offset2,offset3,heading1,heading2,heading3,distance,heading,HOGSEGTotalScore)
       return finialS,distance,heading,HOGSEGTotalScore
    else:
       return 1000,1000,1000,1000

def calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep):
  
    count = 0
    smoothScore = 800000
    offset1 = -1
    offset2 = -1
    offset3 = -1
    heading1 = -1
    heading2 = -1
    heading3 = -1
    print('len : ',len(OffsetArray[initialFrame]))
    print('len1 : ',len(OffsetArray[initialFrame+1]))
    print('len2 : ',len(OffsetArray[initialFrame+2]))
    for i in range(len(OffsetArray[initialFrame])):
        for j in range(len(OffsetArray[initialFrame+1])):
            print(initialFrame,OffsetArray[initialFrame][i]-100,OffsetArray[initialFrame+1][j]-100)
            for k in range(len(OffsetArray[initialFrame+2])):
                for o in range(len(HeadingArray[initialFrame])):
                    for p in range(len(HeadingArray[initialFrame+1])):
                        for u in range(len(HeadingArray[initialFrame+2])):
                            tempScore,d,h,s = calculateSmoothScore(initialFrame+1,1
                            ,OffsetArray[initialFrame][i],OffsetArray[initialFrame+1][j],OffsetArray[initialFrame+2][k],HeadingArray[initialFrame][o],HeadingArray[initialFrame+1][p],HeadingArray[initialFrame+2][u])
                            #print(OffsetArray[0][i],OffsetArray[1][j],OffsetArray[2][k],HeadingArray[0][o],HeadingArray[1][p],HeadingArray[2][u],tempScore)
                            if tempScore<smoothScore:
                                    smoothScore = tempScore
                                    offset1 = OffsetArray[initialFrame][i]
                                    offset2 = OffsetArray[initialFrame+1][j]
                                    offset3 = OffsetArray[initialFrame+2][k]
                                    heading1 = HeadingArray[initialFrame][o]
                                    heading2 = HeadingArray[initialFrame+1][p]
                                    heading3 = HeadingArray[initialFrame+2][u]
                                    #print(offset1,offset2,offset3,smoothScore)
    print('calculateRange: ',initialFrame,offset1-100, offset2-100,offset3-100,heading1-20,heading2-20, heading3-20)
    return offset1, offset2,offset3,heading1,heading2, heading3

def findNextRange(initialStart,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step):
  offsetSort = [offset1, offset2,offset3]
  if offset1 == offset2 and offset1 == offset3:
      if offset1-step >= offsetMinRange[initialStart]:
        offsetMinRange[initialStart] = offset1-step
        offsetMinRange[initialStart+1] = offset1-step
        offsetMinRange[initialStart+2] = offset1-step

      if offset1+step <= offsetMaxRange[initialStart]:
        offsetMaxRange[initialStart] = offset1+step
        offsetMaxRange[initialStart+1] = offset1+step
        offsetMaxRange[initialStart+2] = offset1+step
      
  elif offset1 != offset2 and offset1 != offset3 and offset2 != offset3:
      if min(offsetSort)-step >= offsetMinRange[initialStart]:
        offsetMinRange[initialStart] = min(offsetSort)-step
        offsetMinRange[initialStart+1] = min(offsetSort)-step
        offsetMinRange[initialStart+2] = min(offsetSort)-step

      if max(offsetSort)+step <= offsetMaxRange[initialStart]:
        offsetMaxRange[initialStart] = max(offsetSort)+step
        offsetMaxRange[initialStart+1] = max(offsetSort)+step
        offsetMaxRange[initialStart+2] = max(offsetSort)+step
  else:
      if offset1 == offset2:
        if offset2-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset2-step
            offsetMinRange[initialStart+1] = offset2-step
            offsetMinRange[initialStart+2] = offset2-step

        if offset2+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset2+step
            offsetMaxRange[initialStart+1] = offset2+step
            offsetMaxRange[initialStart+2] = offset2+step

      elif offset1 == offset3:
        if offset3-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset3-step
            offsetMinRange[initialStart+1] = offset3-step
            offsetMinRange[initialStart+2] = offset3-step

        if offset3+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset3+step
            offsetMaxRange[initialStart+1] = offset3+step
            offsetMaxRange[initialStart+2] = offset3+step

      elif offset2 == offset3:
        if offset3-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset3-step
            offsetMinRange[initialStart+1] = offset3-step
            offsetMinRange[initialStart+2] = offset3-step

        if offset3+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset3+step
            offsetMaxRange[initialStart+1] = offset3+step
            offsetMaxRange[initialStart+2] = offset3+step
  return offsetMinRange,offsetMaxRange

def runInitialFrame(initialFrame,NumberOfFrame,step,headingStep,files,TotalScore,offsetMaxRange,offsetMinRange,OffsetArray,HeadingArray,nowOffset,nowHeading):


  OffsetArray[initialFrame].append(nowOffset)
  HeadingArray[initialFrame].append(nowHeading)
  for i in range(initialFrame+1,NumberOfFrame):
      LOGData =  open(LOGPath + str(i) +'.txt', 'r')
      for lines in LOGData:
          if lines[0] == 'r':
              arr = lines.split(':')
              if int(arr[1]) > 0 :
                  offsetMaxRange[i] = (int(arr[1])+100)
              else:
                  offsetMinRange[i] = (int(arr[1])+100)
      for j in range(offsetMinRange[i],offsetMaxRange[i]+1,step):
        OffsetArray[i].append(j)
        print(j-100)
      for j in range(0,40,headingStep):
        HeadingArray[i].append(j)

  #先放好要排序的順序
  offset1, offset2,offset3,heading1,heading2, heading3 = calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep)
  offsetMinRange,offsetMaxRange = findNextRange(initialFrame,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step)
  
  while step >= 0:
    if step >5 :
      step = int(step/2)
    else:
      step = 1
      headingStep = 1
    OffsetArray= []
    HeadingArray= []

    for i in range(len(files)+200):
      OffsetArray.append([])
      HeadingArray.append([])

    OffsetArray[initialFrame].append(nowOffset)
    HeadingArray[initialFrame].append(nowHeading)
    for i in range(initialFrame+1,NumberOfFrame):
      print('minMax:',offsetMinRange[i]-100,offsetMaxRange[i]-100,heading1-20,heading2-20,heading3-20)
      for j in range(offsetMinRange[i],offsetMaxRange[i]+1,step):
        OffsetArray[i].append(j)
        #print(j)

      #處理heading範圍
      allHeading = [heading1,heading2,heading3]
      maxHeading = max(allHeading)
      minHeading = min(allHeading)
      if maxHeading+5 <= 40:
        maxHeading = maxHeading + 5
      else:
        maxHeading = 40 
      if minHeading - 5 >=0:
        minHeading = minHeading -5
      else:
        minHeading = 0

      for j in range(minHeading,maxHeading,headingStep):
        HeadingArray[i].append(j)
        #print(j)
    offset1, offset2,offset3,heading1,heading2, heading3 = calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep)
    offsetMinRange,offsetMaxRange = findNextRange(initialFrame,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step)
    if step ==1 :
      step = -1

  return offset1, offset2,offset3,heading1,heading2, heading3


if __name__ == '__main__':
  demo = ''
  TotalScorePath = "D:/0826/AutoData_demo/20190912_turn_right/temp/data/finialScore/"
  LOGPath = "D:/0826/AutoData_demo/20190912_turn_right/temp/data/LOG/"
  fileNamePath = "D:/0826/AutoData_demo/20190912_turn_right/temp/data/FileName/"
  fixedDataPath = "D:/0826/AutoData_demo/20190912_turn_right/temp_after/"
  #TotalScorePath = "F:/straight/temp_0610_circle3/data/finialScore/"
  #LOGPath = "F:/straight/temp_0610_circle3/data/LOG/"
  #fileNamePath = "F:/straight/temp_0610_circle3/data/FileName/"
  files = listdir(TotalScorePath)
  TotalScore = []
  offsetMaxRange = []
  offsetMinRange = []
  OffsetArray = []
  HeadingArray = []
  finialIndexArray = []
  for i in range(len(files)+200):
     TotalScore.append([])
     OffsetArray.append([])
     HeadingArray.append([])
     offsetMaxRange.append(-1)
     offsetMinRange.append(-1)
     finialIndexArray.append(finialIndex())
     for j in range(201) :
         TotalScore[i].append([])
         for k in range(41) :
            TotalScore[i][j].append(Score(500))

  finialSortScore = []
  for i in range(len(files)+200):
    finialSortScore.append([])
  frameSmooth = []
  for i in range(len(files)+200):
    frameSmooth.append([])

  
  for f in files:
     ScorePath = open(TotalScorePath + f, 'r')
     #print(f)
     for lines in ScorePath:
         arr= lines.split(',')
         #print(arr[0])
         temp = Score(float(arr[3]))
         #temp.fileName = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].fileName
         #print(int(arr[0]),int(arr[1])+100,int(arr[2])+20) 
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20] = temp
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].HogScore = float(arr[6])
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].SegScore = float(arr[4])
  
  files = listdir(fileNamePath)
  for f in files:
    fileNameData =  open(fileNamePath + f, 'r')
    for lines in fileNameData:
        arr1 = lines.split('/')
        if len(arr1) > 4:
          arr= arr1[6].split('_')
        else:
          arr = lines.split('_')
        #arr = lines.split('_')
        if int(arr[1]) <=100 and int(arr[1]) >=-100:
          TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].x = float(arr[3])
          TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].y = float(arr[4])
          TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = float(arr[5])
          #if float(arr[5]) <0 :
          #  TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = 360 + float(arr[5])
          #else:
          #  TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = float(arr[5])

  fileNameData =  open(fixedDataPath+'fixedData.txt', 'r')
  fixedIndex = []
  fixedOffset = []
  fixHeading = []
  for lines in fileNameData:
    arr = lines.split(' ')
    fixedIndex.append(int(arr[0]))
    fixedOffset.append(int(arr[1]))
    fixHeading.append(int(arr[2]))
  fixedIndex.append( int (fixedIndex[0] + len(files)-1) )
  output += str(fixedIndex[0]-1) + ' '+str(fixedOffset[0])+' '+str(fixHeading[0])+' '+'\n'

  for FixedindexCount in range(len(fixedIndex)-1):
    initialFrameIndex = fixedIndex[FixedindexCount]
    initialFrame = initialFrameIndex
    NumberOfFrame = initialFrameIndex + 3
    smoothTermScore = []
    #output += 'round ' +str(round)+' '+ str(initialFrame) + '\n'
    smoothTermScore.append(1000)
    
    nowOffset = fixedOffset[FixedindexCount] +100
    nowHeading =fixHeading[FixedindexCount] +20

    step = 20
    headingStep = 2
    offset1, offset2,offset3,heading1,heading2, heading3 = runInitialFrame(initialFrame,NumberOfFrame,step,headingStep,files,TotalScore,offsetMaxRange,offsetMinRange,OffsetArray,HeadingArray,nowOffset,nowHeading)

    nextOffset = offset2
    nextHeading = heading2

    finialIndexArray[initialFrame].index = nowOffset
    finialIndexArray[initialFrame].heading = nowHeading
    finialIndexArray[initialFrame+1].index = offset2
    finialIndexArray[initialFrame+1].heading = heading2
    #print('inputData: ',initialFrame,finialIndexArray[initialFrame].Index)

    finialIndexArray[initialFrame].frontIndex = nowOffset
    finialIndexArray[initialFrame].frontHeading = nowHeading
    finialIndexArray[initialFrame+1].frontIndex = offset2
    finialIndexArray[initialFrame+1].frontHeading = heading2

    #固定一個點後延伸
    TotalSmoothScore = 0
    print(initialFrame,nowOffset-100,nowHeading-20)
    print(initialFrame+1,nextOffset-100,nextHeading-20)
    output += str(initialFrame) + ' '+str(nowOffset-100)+' '+str(nowHeading-20)+' '+'\n'
    output += str(initialFrame+1) + ' '+str(nextOffset-100)+' '+str(nextHeading-20)+' '+'\n'
    for i in range(initialFrame+2 ,fixedIndex[FixedindexCount+1]):
        Score = 8000
        finialHeading = 0
        finialOffset = 0
        d = 0
        h = 0
        s = 0
        minRange = -1
        maxRange = -1
        LOGData =  open(LOGPath + str(i) +'.txt', 'r')
        for lines in LOGData:
            if lines[0] == 'r':
                arr = lines.split(':')
                if int(arr[1]) > 0 :
                  maxRange = int(arr[1])-1+100
                else:
                  minRange =  int(arr[1])+100
        #print(minRange,maxRange)
        for j in range(minRange,maxRange+1):
            for k in range(0,40):
              #print('finial: ',i,nowOffset-100,nowHeading-20,nextOffset-100,nextHeading-20,j-100,k-20)
              if TotalScore[i-2][nowOffset][nowHeading].score <=10 and TotalScore[i-1][nextOffset][nextHeading].score<=10 and TotalScore[i][j][k].score<=10:
                temp,d,h,s = calculateSmoothScore(i-2,1,nowOffset,nextOffset,j,nowHeading,nextHeading,k)
                if Score > temp:
                    #print(i,j,k,temp,Score)
                    Score = temp
                    finialHeading = k
                    finialOffset = j
              #else:c
                #if i == 41:
                # print(i,i+1,i-1,TotalScore[i][preOffset][preHeading].score,TotalScore[i+1][nowOffset][nowHeading].score,TotalScore[i-1][j][k].score)
        nowOffset = nextOffset
        nowHeading = nextHeading
        nextOffset = finialOffset
        nextHeading = finialHeading

        TotalSmoothScore += Score
        print(i,finialOffset-100,finialHeading-20,Score,d,h,s)
        finialIndexArray[i].index = finialOffset
        finialIndexArray[i].heading = finialHeading

        output += str(i) + ' '+str(finialOffset-100)+' '+str(finialHeading-20)+' '+str(Score)+'\n'
          
    #向前延伸

    nowOffset = offset2
    nowHeading = heading2
    nextOffset = fixedOffset[FixedindexCount] +100
    nextHeading =fixHeading[FixedindexCount] +20
    
    print('front')
    TotalSmoothScore = 0
    print(initialFrame+1,nowOffset-100,nowHeading-20)
    print(initialFrame,nextOffset-100,nextHeading-20)
    output += str(initialFrame) + ' '+str(nowOffset-100)+' '+str(nowHeading-20)+' '+'\n'
    output += str(initialFrame+1) + ' '+str(nextOffset-100)+' '+str(nextHeading-20)+' '+'\n'
    if FixedindexCount-1 < 0 :
      finialIndex = -1
    else:
      finialIndex = fixedIndex[FixedindexCount-1]+1
    for i in range(initialFrame-1 ,finialIndex,-1):
        Score = 8000
        finialHeading = 0
        finialOffset = 0
        d = 0
        h = 0
        s = 0
        minRange = -1
        maxRange = -1
        LOGData =  open(LOGPath + str(i) +'.txt', 'r')
        for lines in LOGData:
            if lines[0] == 'r':
                arr = lines.split(':')
                if int(arr[1]) > 0 :
                  maxRange = int(arr[1])-1+100
                else:
                  minRange =  int(arr[1])+100
        #print(minRange,maxRange)
        for j in range(minRange,maxRange):
            for k in range(0,40):
              #print('finial: ',i,nowOffset-100,nowHeading-20,nextOffset-100,nextHeading-20,j-100,k-20)
              if TotalScore[i+2][nowOffset][nowHeading].score <=10 and TotalScore[i+1][nextOffset][nextHeading].score<=10 and TotalScore[i][j][k].score<=10:
                temp,d,h,s = calculateSmoothScore(i+2,-1,nowOffset,nextOffset,j,nowHeading,nextHeading,k)
                if Score > temp:
                    #print(i,j,k,temp,Score)
                    Score = temp
                    finialHeading = k
                    finialOffset = j
              #else:c
                #if i == 41:
                # print(i,i+1,i-1,TotalScore[i][preOffset][preHeading].score,TotalScore[i+1][nowOffset][nowHeading].score,TotalScore[i-1][j][k].score)
        nowOffset = nextOffset
        nowHeading = nextHeading
        nextOffset = finialOffset
        nextHeading = finialHeading
        finialIndexArray[i].frontIndex = finialOffset
        finialIndexArray[i].frontHeading = finialHeading

        TotalSmoothScore += Score
        print(i,finialOffset-100,finialHeading-20,Score,d,h,s)
        output += str(i) + ' '+str(finialOffset-100)+' '+str(finialHeading-20)+' '+str(Score)+'\n'
    #print(TotalSmoothScore)
    #smoothTermScore[len(smoothTermScore)-1] += TotalSmoothScore


    #print(TotalSmoothScore)
    #smoothTermScore[len(smoothTermScore)-1] += TotalSmoothScore




  #for i in range(len(smoothTermScore)):
    #print(smoothTermScore[i])
    #output += str(i*10+1)+','+str(smoothTermScore[i]) + '\n'

  for i in range(len(finialIndexArray)):
    #print(80,finialIndexArray[80].index-100,finialIndexArray[80].heading-20,finialIndexArray[80].frontIndex-100,finialIndexArray[80].frontHeading-20)
    if finialIndexArray[i].index != -1 and finialIndexArray[i].frontIndex != -1:
      print(i,'o: ',finialIndexArray[i].index-100,finialIndexArray[i].frontIndex-100,' h:',finialIndexArray[i].heading-20,finialIndexArray[i].frontHeading-20)
    elif finialIndexArray[i].index != -1 and finialIndexArray[i].frontIndex == -1:
      print(i,'o: ',finialIndexArray[i].index-100,finialIndexArray[i].index-100,' h:',finialIndexArray[i].heading-20,finialIndexArray[i].heading-20)

  filenameData= fixedDataPath +"fixed_data_result.txt"
  with open(filenameData, 'w') as file:
    file.write(output)

        

