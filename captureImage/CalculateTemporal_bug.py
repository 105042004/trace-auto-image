import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

class positionHeadingData():
 def __init__(self,offset,heading):
  self.offset = offset
  self.heading = heading
  self.score = -1
  self.smoothScore = -1



class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.HogScore = 5000
  self.SegScore = -1
  self.x = 0
  self.y = 0
  self.heading = 0


output = ''
outputTime = ''

def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2

def calculateSmoothScore(index,offset1,offset2,offset3,heading1,heading2,heading3,termWeight,smoothWeight):
    lat =TotalScore[index][offset1][heading1].x
    lon = TotalScore[index][offset1][heading1].y
    nextLat = TotalScore[index+1][offset2][heading2].x
    nextLon = TotalScore[index+1][offset2][heading2].y
    preLat = TotalScore[index-1][offset3][heading3].x
    preLon = TotalScore[index-1][offset3][heading3].y
    dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
    dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
    heading = abs(TotalScore[index+1][offset2][heading2].heading - TotalScore[index][offset1][heading1].heading)
    heading += abs(TotalScore[index][offset1][heading1].heading - TotalScore[index-1][offset3][heading3].heading)
    distance = dis1+dis3
    if TotalScore[index][offset1][heading1].score <5 and TotalScore[index+1][offset2][heading2].score<5 and TotalScore[index-1][offset3][heading3].score<5:
       HOGSEGTotalScore = TotalScore[index][offset1][heading1].score +TotalScore[index+1][offset2][heading2].score +TotalScore[index-1][offset3][heading3].score
       disScore = dealScore(distance,1000)
       headingScore = dealScore(heading,100)
       Score = dealScore(HOGSEGTotalScore,1.0)
       finialS =  (termWeight/10)*((smoothWeight/10)*disScore + (1-(smoothWeight/10))*headingScore) + (1-(termWeight/10))*Score
       #if index == 41:
       #  print(index,index+1,index-1,offset1,offset2,offset3,heading1,heading2,heading3,distance,heading,HOGSEGTotalScore)
       return finialS,distance,heading,HOGSEGTotalScore
    else:
       return 1000,1000,1000,1000



def calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep,termWeight,smoothWeight):
  
    count = 0
    smoothScore = 800000
    offset1 = -1
    offset2 = -1
    offset3 = -1
    heading1 = -1
    heading2 = -1
    heading3 = -1
    startR = time.time()
    print('len : ',initialFrame,len(OffsetArray[initialFrame-1]),len(OffsetArray[initialFrame]),len(OffsetArray[initialFrame+1]))
    for i in range(len(OffsetArray[initialFrame])):
        for j in range(len(OffsetArray[initialFrame+1])):
            print(initialFrame,OffsetArray[initialFrame][i],OffsetArray[initialFrame+1][j])
            for k in range(len(OffsetArray[initialFrame-1])):
                for o in range(len(HeadingArray[initialFrame])):
                    for p in range(len(HeadingArray[initialFrame+1])):
                        for u in range(len(HeadingArray[initialFrame-1])):
                            tempScore,d,h,s = calculateSmoothScore(initialFrame+1
                            ,OffsetArray[initialFrame][i],OffsetArray[initialFrame+1][j],OffsetArray[initialFrame-1][k],HeadingArray[initialFrame][o],HeadingArray[initialFrame+1][p],HeadingArray[initialFrame-1][u],termWeight,smoothWeight)
                            #print(OffsetArray[0][i],OffsetArray[1][j],OffsetArray[2][k],HeadingArray[0][o],HeadingArray[1][p],HeadingArray[2][u],tempScore)
                            if tempScore<smoothScore:
                                    smoothScore = tempScore
                                    offset1 = OffsetArray[initialFrame][i]
                                    offset2 = OffsetArray[initialFrame+1][j]
                                    offset3 = OffsetArray[initialFrame-1][k]
                                    heading1 = HeadingArray[initialFrame][o]
                                    heading2 = HeadingArray[initialFrame+1][p]
                                    heading3 = HeadingArray[initialFrame-1][u]
                                    #print(offset1,offset2,offset3,smoothScore)
    endR = time.time()
    elapsed = endR - startR
    print ("calculateRange: ", elapsed, "seconds.")
    outputTime = "calculateRange: "+ str(elapsed)+"seconds.\n"
    filenameData= "E:/0826/AutoData/temp_after/time.txt"
    with open(filenameData, 'a') as file:
        file.write(outputTime)
    
    print(offset1, offset2,offset3,heading1,heading2, heading3)
    return offset1, offset2,offset3,heading1,heading2, heading3

def findNextRange(initialStart,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step):
  offsetSort = [offset1, offset2,offset3]
  if offset1 == offset2 and offset1 == offset3:
      if offset1-step >= offsetMinRange[initialStart]:
        offsetMinRange[initialStart] = offset1-step
        offsetMinRange[initialStart+1] = offset1-step
        offsetMinRange[initialStart-1] = offset1-step

      if offset1+step <= offsetMaxRange[initialStart]:
        offsetMaxRange[initialStart] = offset1+step
        offsetMaxRange[initialStart+1] = offset1+step
        offsetMaxRange[initialStart-1] = offset1+step
      
  elif offset1 != offset2 and offset1 != offset3 and offset2 != offset3:
      if min(offsetSort)-step >= offsetMinRange[initialStart]:
        offsetMinRange[initialStart] = min(offsetSort)-step
        offsetMinRange[initialStart+1] = min(offsetSort)-step
        offsetMinRange[initialStart-1] = min(offsetSort)-step

      if max(offsetSort)+step <= offsetMaxRange[initialStart]:
        offsetMaxRange[initialStart] = max(offsetSort)+step
        offsetMaxRange[initialStart+1] = max(offsetSort)+step
        offsetMaxRange[initialStart-1] = max(offsetSort)+step
  else:
      if offset1 == offset2:
        if offset2-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset2-step
            offsetMinRange[initialStart+1] = offset2-step
            offsetMinRange[initialStart-1] = offset2-step

        if offset2+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset2+step
            offsetMaxRange[initialStart+1] = offset2+step
            offsetMaxRange[initialStart-1] = offset2+step

      elif offset1 == offset3:
        if offset3-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset3-step
            offsetMinRange[initialStart+1] = offset3-step
            offsetMinRange[initialStart-1] = offset3-step

        if offset3+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset3+step
            offsetMaxRange[initialStart+1] = offset3+step
            offsetMaxRange[initialStart-1] = offset3+step

      elif offset2 == offset3:
        if offset3-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset3-step
            offsetMinRange[initialStart+1] = offset3-step
            offsetMinRange[initialStart-1] = offset3-step

        if offset3+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset3+step
            offsetMaxRange[initialStart+1] = offset3+step
            offsetMaxRange[initialStart-1] = offset3+step
  return offsetMinRange,offsetMaxRange

def runInitialFrame(initialFrame,NumberOfFrame,step,headingStep,files,TotalScore,offsetMaxRange,offsetMinRange,OffsetArray,HeadingArray,termWeight,smoothWeight):


  for i in range(initialFrame-1,NumberOfFrame):
      LOGData =  open(LOGPath + str(i) +'.txt', 'r')
      for lines in LOGData:
          if lines[0] == 'r':
              arr = lines.split(':')
              if int(arr[1]) > 0 :
                  offsetMaxRange[i] = (int(arr[1])+100)
              else:
                  offsetMinRange[i] = (int(arr[1])+100)
      for j in range(offsetMinRange[i],offsetMaxRange[i]+1,step):
        OffsetArray[i].append(j)
        print(i,j)
      for j in range(0,40,headingStep):
        HeadingArray[i].append(j)

  #先放好要排序的順序
  offset1, offset2,offset3,heading1,heading2, heading3 = calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep,termWeight,smoothWeight)
  offsetMinRange,offsetMaxRange = findNextRange(initialFrame,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step)
  
  while step >= 0:
    if step >5 :
      step = int(step/2)
    else:
      step = 1
      headingStep = 1
    OffsetArray= []
    HeadingArray= []
    for i in range(len(files)+200):
      OffsetArray.append([])
      HeadingArray.append([])


    for i in range(initialFrame-1,NumberOfFrame):
      print('minMax:',offsetMinRange[i],offsetMaxRange[i],heading1,heading2,heading3,termWeight,smoothWeight)
      for j in range(offsetMinRange[i],offsetMaxRange[i]+1,step):
        OffsetArray[i].append(j)
        print(i,j)

      #處理heading範圍
      allHeading = [heading1,heading2,heading3]
      maxHeading = max(allHeading)
      minHeading = min(allHeading)
      if maxHeading+5 <= 40:
        maxHeading = maxHeading + 5
      else:
        maxHeading = 40 
      if minHeading - 5 >=0:
        minHeading = minHeading -5
      else:
        minHeading = 0

      for j in range(minHeading,maxHeading,headingStep):
        HeadingArray[i].append(j)
        #print(j)
    offset1, offset2,offset3,heading1,heading2, heading3 = calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep,termWeight,smoothWeight)
    offsetMinRange,offsetMaxRange = findNextRange(initialFrame,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step)
    if step ==1 :
      step = -1

  return offset1, offset2,offset3,heading1,heading2, heading3



if __name__ == '__main__':
  startall = time.time()
  #roundRound代表要跑哪幾個demo
  for roundRound in range(1,2):
       
      #TotalScorePath = "D:/lab/ITRI/demo/20190426/demo"+str(roundRound)+"/temp/data/finialScore/"
      #LOGPath = "D:/lab/ITRI/demo/20190426/demo"+str(roundRound)+"/temp/data/LOG/"
      #fileNamePath = "D:/lab/ITRI/demo/20190426/demo"+str(roundRound)+"/temp/data/FileName/"
      #outputPath = "D:/lab/ITRI/demo/20190426/demo"+str(roundRound)+"/temp"
      TotalScorePath = "E:/0826/AutoData/temp/data/finialScore/"
      LOGPath = "E:/0826/AutoData/temp/data/LOG/"
      fileNamePath = "E:/0826/AutoData/temp/data/FileName/"
      outputPath = "E:/0826/AutoData/temp_after/"
      
      print(outputPath)
      #TotalScorePath = "F:/straight/temp_0610_circle3/data/finialScore/"
      #LOGPath = "F:/straight/temp_0610_circle3/data/LOG/"
      #fileNamePath = "F:/straight/temp_0610_circle3/data/FileName/"
      files = listdir(TotalScorePath)
      TotalScore = []
      for i in range(len(files)+200):
        TotalScore.append([])
        for j in range(201) :
            TotalScore[i].append([])
            for k in range(41) :
                TotalScore[i][j].append(Score(500))

      for f in files:
        ScorePath = open(TotalScorePath + f, 'r')
        #print(f)
        for lines in ScorePath:
            arr= lines.split(',')
            #print(arr[0])
            temp = Score(float(arr[3]))
            #temp.fileName = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].fileName
            #print(int(arr[0]),int(arr[1])+100,int(arr[2])+20) 
            TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20] = temp
            TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].HogScore = float(arr[6])
            TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].SegScore = float(arr[4])
      
      files = listdir(fileNamePath)
      for f in files:
        fileNameData =  open(fileNamePath + f, 'r')
        for lines in fileNameData:
            arr1 = lines.split('/')
            if len(arr1) > 4:
              arr= arr1[5].split('_')
            else:
              arr = lines.split('_')
            if int(arr[1]) <=100 and int(arr[1]) >=-100:
              TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].x = float(arr[3])
              TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].y = float(arr[4])
              TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = float(arr[5])
              #if float(arr[5]) <0 :
              #  TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = 360 + float(arr[5])
              #else:
              #  TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = float(arr[5])
      #termWeight和smoothWeight代表權重的部分，目前已經設定好不用變更
      for termWeight in range(3,4):
        for smoothWeight in range(8,9):
          output = ''
          offsetMaxRange = []
          offsetMinRange = []
          OffsetArray = []
          HeadingArray = []
          for i in range(len(files)+200):
            OffsetArray.append([])
            HeadingArray.append([])
            offsetMaxRange.append(-1)
            offsetMinRange.append(-1)

          finialSortScore = []
          for i in range(len(files)+200):
            finialSortScore.append([])
          frameSmooth = []
          for i in range(len(files)+200):
            frameSmooth.append([])

          if TotalScore[1][0][0].score == 500:
            initialFrameIndex = 1
          else:
            initialFrameIndex = 1

          initialFrame = initialFrameIndex +1
          NumberOfFrame = initialFrameIndex +4 
          smoothTermScore = []
          length = len(files)/10

          for round in range(0,int(length)):
            output += 'round ' +str(round)+' '+ str(initialFrame) + '\n'
            step = 20
            headingStep = 2
            smoothTermScore.append(1000)
            start = time.time()
            offset1, offset2,offset3,heading1,heading2, heading3 = runInitialFrame(initialFrame,NumberOfFrame,step,headingStep,files,TotalScore,offsetMaxRange,offsetMinRange,OffsetArray,HeadingArray,termWeight,smoothWeight)
            end = time.time()
            elapsed = end - start
            outputTime = "runInitialFrame: "+ str(elapsed)+"seconds.\n"
            filenameData= "E:/0826/AutoData/temp_after/time.txt"
            with open(filenameData, 'a') as file:
                file.write(outputTime)
            print ("runInitialFrame: ", elapsed, "seconds.")
            #demo5 (16,16,16,21,21,21)
            #固定三個點後延伸
            #offset1 = 87
            #heading1 = 25
            #offset2 = 87
            #heading2 = 25
            #offset3 = 87
            #heading3 = 25

            preOffset = offset1
            preHeading = heading1
            nowOffset =offset2
            nowHeading = heading2
            TotalSmoothScore = 0
            start = time.time()
            for i in range(initialFrame-1,initialFrameIndex,-1):
                Score = 8000
                finialHeading = 0
                finialOffset = 0
                d = 0
                h = 0
                s = 0
                minRange = -1
                maxRange = -1
                LOGData =  open(LOGPath + str(i) +'.txt', 'r')
                for lines in LOGData:
                    if lines[0] == 'r':
                        arr = lines.split(':')
                        if int(arr[1]) > 0 :
                          maxRange = int(arr[1])-1+100
                        else:
                          minRange =  int(arr[1])+100
                #print(minRange,maxRange)
                for j in range(minRange,maxRange):
                    for k in range(0,40):
                      if TotalScore[i+1][preOffset][preHeading].score <=10 and TotalScore[i+2][nowOffset][nowHeading].score<=10 and TotalScore[i][j][k].score<=10:
                        temp,d,h,s = calculateSmoothScore(i+1,preOffset,nowOffset,j,preHeading,nowHeading,k,termWeight,smoothWeight)
                        #print(temp)
                        if Score > temp:
                            #print(i,j,k,temp,Score)
                            Score = temp
                            finialHeading = k
                            finialOffset = j
                nowOffset = preOffset
                nowHeading = preHeading
                preOffset = finialOffset
                preHeading = finialHeading
                TotalSmoothScore += Score
                print(i,finialOffset-100,finialHeading-20,Score)
                output += str(i) + ','+str(finialOffset-100)+','+str(finialHeading-20)+','+str(Score)+'\n'
            print(TotalSmoothScore)
            smoothTermScore[len(smoothTermScore)-1] = TotalSmoothScore

            preOffset = offset1
            preHeading = heading1
            nowOffset = offset2
            nowHeading = heading2
            TotalSmoothScore = 0
            for i in range(initialFrame + 2,initialFrameIndex + len(files)-1):
                Score = 8000
                finialHeading = 0
                finialOffset = 0
                d = 0
                h = 0
                s = 0
                minRange = -1
                maxRange = -1
                LOGData =  open(LOGPath + str(i) +'.txt', 'r')
                for lines in LOGData:
                    if lines[0] == 'r':
                        arr = lines.split(':')
                        if int(arr[1]) > 0 :
                          maxRange = int(arr[1])-1+100
                        else:
                          minRange =  int(arr[1])+100
                #print(minRange,maxRange)
                for j in range(minRange,maxRange):
                    for k in range(0,40):
                      if TotalScore[i-2][preOffset][preHeading].score <=10 and TotalScore[i][j][k].score<=10 and TotalScore[i-1][nowOffset][nowHeading].score<=10:
                        temp,d,h,s = calculateSmoothScore(i-1,nowOffset,j,preOffset,nowHeading,k,preHeading,termWeight,smoothWeight)
                        #print(temp)
                        if Score > temp:
                            #print(i,j,k,temp,Score)
                            Score = temp
                            finialHeading = k
                            finialOffset = j
                      #else:
                        #if i == 41:
                        # print(i,i+1,i-1,TotalScore[i][preOffset][preHeading].score,TotalScore[i+1][nowOffset][nowHeading].score,TotalScore[i-1][j][k].score)
                preOffset = nowOffset
                preHeading = nowHeading
                nowOffset = finialOffset
                nowHeading = finialHeading
                TotalSmoothScore += Score
                print(i,finialOffset-100,finialHeading-20,Score)
                output += str(i) + ','+str(finialOffset-100)+','+str(finialHeading-20)+','+str(Score)+'\n'
            end = time.time()
            elapsed = end - start
            print ("calculate: ", elapsed, "seconds.")
            outputTime = "calculate: "+ str(elapsed)+"seconds.\n"
            filenameData= "E:/0826/AutoData/temp_after/time.txt"
            with open(filenameData, 'a') as file:
                file.write(outputTime)
            print(TotalSmoothScore)
            smoothTermScore[len(smoothTermScore)-1] += TotalSmoothScore
            initialFrame +=10
            NumberOfFrame += 10


           

          for ii in range(len(smoothTermScore)):
            print(smoothTermScore[ii])
            output += str(i*10+1)+','+str(smoothTermScore[ii]) +","+str(termWeight)+"_"+str(smoothWeight)+ '\n'

          filenameData= outputPath +"smoothTermResult_"+str(termWeight)+"_"+str(smoothWeight)+".txt"
          with open(filenameData, 'w') as file:
            file.write(output)
  endall = time.time()
  elapsed = endall - startall
  outputTime = "all: "+ str(elapsed)+"seconds.\n"
  filenameData= "E:/0826/AutoData/temp_after/time.txt"
  with open(filenameData, 'a') as file:
      file.write(outputTime)


                

