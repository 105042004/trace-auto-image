import numpy as np
import argparse
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, isdir, join

inputPath = "D:/lab/ITRI/demo/20190426/demo5/smoothTermWithFixedData/outputGT.txt"
outputPath = "D:/lab/ITRI/demo/20190426/demo5/smoothTermWithFixedData/fixedData.txt"
fileData = "D:/lab/ITRI/demo/20190426/demo5/temp/data/FileName/"
class GTIndex():
 def __init__(self,offsetIndex,headingIndex):
  self.offsetIndex = offsetIndex
  self.headingIndex = headingIndex

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2

if __name__ == '__main__':

    InputData = open(inputPath, 'r')
    GTList = []
    output = ''
    for lines in InputData:
        arr= lines.split(',')
        temp = GTIndex(-1,-1)
        IndexData = open(fileData+str(arr[0])+'.txt', 'r')
        minDis = 9999999999
        minHeading = 999999999
        for Indexlines in IndexData:
            arr2= Indexlines.split('_')
            dis1,dis2 = calculateDis(float(arr[1]),float(arr[2]),float(arr2[3]),float(arr2[4]))
            if dis1 < minDis:
                temp.offsetIndex = int(arr2[1])
                minDis = dis1
            if abs(float(arr[3])-float(arr2[5])) < minHeading:
                 temp.headingIndex = int(arr2[2])
                 minHeading = abs(float(arr[3])-float(arr2[5]))
        output += str(arr[0])+' '+str(temp.offsetIndex)+' '+str(temp.headingIndex)+'\n'
        print(arr[0],temp.offsetIndex,temp.headingIndex,float(arr[1]),float(arr[2]),float(arr2[3]),float(arr2[4]))
    print(output)
    filenameData= outputPath
    with open(outputPath, 'w') as file:
       file.write(output)

