import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

def calculatePixelRatio(img_seg, img_floor,weight):
   
   w = int(img_seg.shape[0]/5)
   h = int(img_seg.shape[1]/5)
   img_seg = cv2.resize(img_seg,(h,w) , interpolation=cv2.INTER_CUBIC)
   img_floor = cv2.resize(img_floor, (h,w), interpolation=cv2.INTER_CUBIC)
   
   Gray_Color = [
        ([130,130,130], [140,140,140])#黄色范围~这个是我自己试验的范围，可根据实际情况自行调整~注意：数值按[b,g,r]排布
    ]

   blue_Color = [
        ([190,92,0], [210,112,10])#黄色范围~这个是我自己试验的范围，可根据实际情况自行调整~注意：数值按[b,g,r]排布
    ]

   purple_Color = [
        ([245,0,153], [255,10,173])#黄色范围~这个是我自己试验的范围，可根据实际情况自行调整~注意：数值按[b,g,r]排布
    ]
   
   sp = img_seg.shape
   width = sp[0]
   height = sp[1]
   countGray = 0
   countBlue = 0
   countPurple = 0
   countRed = 0
   allGray = 0
   weightBlue = 0.5
   weightPurple = 0.5
   weightRed = 1

   for i in range(int(width/2),width):
       for j in range(0,height):
         data = img_seg[i, j]
         data1 = img_floor[i, j]
         if(data[0] ==140  and data[1] == 140 and data[2] == 140):
           allGray = allGray + 1
           if not (data1[0] ==0 and data1[1] == 0 and data1[2] == 0):
             countGray = countGray+1

         elif (data[0] ==200  and data[1] == 102 and data[2] == 0):
           if not (data1[0] ==0 and data1[1] == 0 and data1[2] == 0):
             countBlue = countBlue+1

         elif (data[0] ==255  and data[1] == 0 and data[2] == 163):
           if not (data1[0] ==0 and data1[1] == 0 and data1[2] == 0):
             countPurple = countPurple+1

         elif (data[0] ==120  and data[1] == 120 and data[2] == 180):
           if not (data1[0] ==0 and data1[1] == 0 and data1[2] == 0):
             countRed = countRed+1

   pixel = countGray + weightBlue*countBlue + weightPurple*countPurple- weightRed * countRed
   allPixel = allGray + countBlue + countPurple
   
   return [pixel,allPixel,pixel/allPixel]
   


def run(mypath,segPath,outputPath):
  # 讀取圖檔
 
  outputStr = ""
  # 取得所有檔案與子目錄名稱
  files = listdir(mypath)

  for f in range(0,len(files)):
    arr = files[f].split('_')
    #print(f)
    if True:
      img_seg = cv2.imread(segPath+str(arr[0])+'.png')
      img_floor = cv2.imread(mypath+str(files[f]))
      output = calculatePixelRatio(img_seg,img_floor,0.5)
      strList = str(arr[0])+","+str(arr[1]) +","+str(arr[2])+","+ str(output[0])+","+str(output[1])+","+str(output[2])+"\n"
      print(strList)
      path = outputPath+str(arr[0])+'.txt'
      with open(path, 'a') as file:
        file.write(strList)

 
