import threading
import time
import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

class positionHeadingData():
 def __init__(self,offset,heading):
  self.offset = offset
  self.heading = heading
  self.score = -1
  self.smoothScore = -1



class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.HogScore = 5000
  self.SegScore = -1
  self.x = 0
  self.y = 0
  self.heading = 0


def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def calculateDis(lat,lon,nextLat,nextLon):
    vector1 = np.array([lat,lon,0])
    vector2 = np.array([nextLat,nextLon,0])
        
    op1=np.sqrt(np.sum(np.square(vector1-vector2)))
    op2=np.linalg.norm(vector1-vector2)
    return op1,op2

def calculateSmoothScore(index,offset1,offset2,offset3,heading1,heading2,heading3):
    lat =TotalScore[index][offset1][heading1].x
    lon = TotalScore[index][offset1][heading1].y
    nextLat = TotalScore[index+1][offset2][heading2].x
    nextLon = TotalScore[index+1][offset2][heading2].y
    preLat = TotalScore[index-1][offset3][heading3].x
    preLon = TotalScore[index-1][offset3][heading3].y
    dis1,dis2 = calculateDis(lat,lon,nextLat,nextLon)
    dis3,dis4 = calculateDis(lat,lon,preLat,preLon)
    heading = abs(TotalScore[index+1][offset2][heading2].heading - TotalScore[index][offset1][heading1].heading)
    heading += abs(TotalScore[index][offset1][heading1].heading - TotalScore[index-1][offset3][heading3].heading)
    distance = dis1+dis3
    HOGSEGTotalScore = TotalScore[index][offset1][heading1].score +TotalScore[index+1][offset2][heading2].score +TotalScore[index-1][offset3][heading3].score
    #print(distance,heading,HOGSEGTotalScore)
    disScore = dealScore(distance,500)
    headingScore = dealScore(heading,15)
    Score = dealScore(HOGSEGTotalScore,0.7)
    finialS =  0.3*(0.5*disScore + 0.5*headingScore) + 0.7*Score 
    return finialS


def calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep):
  
    count = 0
    smoothScore = 800000
    offset1 = 0
    offset2 = 0
    offset3 = 0
    heading1 = 0
    heading2 = 0
    heading3 = 0
    for i in range(len(OffsetArray[initialFrame])):
        for j in range(len(OffsetArray[initialFrame+1])):
            print(OffsetArray[initialFrame][i],OffsetArray[initialFrame+1][j])
            for k in range(len(OffsetArray[initialFrame+2])):
                for o in range(len(HeadingArray[initialFrame])):
                    for p in range(len(HeadingArray[initialFrame+1])):
                        for u in range(len(HeadingArray[initialFrame+2])):
                            tempScore = calculateSmoothScore(initialFrame+1
                            ,OffsetArray[initialFrame][i],OffsetArray[initialFrame+1][j],OffsetArray[initialFrame+2][k],HeadingArray[initialFrame][o],HeadingArray[initialFrame][p],HeadingArray[initialFrame][u])
                            #print(OffsetArray[0][i],OffsetArray[1][j],OffsetArray[2][k],HeadingArray[0][o],HeadingArray[1][p],HeadingArray[2][u],tempScore)
                            if tempScore<smoothScore:
                                    smoothScore = tempScore
                                    offset1 = OffsetArray[initialFrame][i]
                                    offset2 = OffsetArray[initialFrame+1][j]
                                    offset3 = OffsetArray[initialFrame+2][k]
                                    heading1 = HeadingArray[initialFrame][o]
                                    heading2 = HeadingArray[initialFrame+1][p]
                                    heading3 = HeadingArray[initialFrame+2][u]
    print(offset1, offset2,offset3,heading1,heading2, heading3)
    return offset1, offset2,offset3,heading1,heading2, heading3

def findNextRange(initialStart,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step):
  offsetSort = [offset1, offset2,offset3]
  if offset1 == offset2 and offset1 == offset3:
      if offset1-step >= offsetMinRange[initialStart]:
        offsetMinRange[initialStart] = offset1-step
        offsetMinRange[initialStart+1] = offset1-step
        offsetMinRange[initialStart+2] = offset1-step

      if offset1+step <= offsetMaxRange[initialStart]:
        offsetMaxRange[initialStart] = offset1+step
        offsetMaxRange[initialStart+1] = offset1+step
        offsetMaxRange[initialStart+2] = offset1+step
      
  elif offset1 != offset2 and offset1 != offset3 and offset2 != offset3:
      if min(offsetSort)-step >= offsetMinRange[initialStart]:
        offsetMinRange[initialStart] = min(offsetSort)-step
        offsetMinRange[initialStart+1] = min(offsetSort)-step
        offsetMinRange[initialStart+2] = min(offsetSort)-step

      if max(offsetSort)+step <= offsetMaxRange[initialStart]:
        offsetMaxRange[initialStart] = max(offsetSort)+step
        offsetMaxRange[initialStart+1] = max(offsetSort)+step
        offsetMaxRange[initialStart+2] = max(offsetSort)+step
  else:
      if offset1 == offset2:
        if offset2-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset2-step
            offsetMinRange[initialStart+1] = offset2-step
            offsetMinRange[initialStart+2] = offset2-step

        if offset2+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset2+step
            offsetMaxRange[initialStart+1] = offset2+step
            offsetMaxRange[initialStart+2] = offset2+step

      elif offset1 == offset3:
        if offset3-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset3-step
            offsetMinRange[initialStart+1] = offset3-step
            offsetMinRange[initialStart+2] = offset3-step

        if offset3+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset3+step
            offsetMaxRange[initialStart+1] = offset3+step
            offsetMaxRange[initialStart+2] = offset3+step

      elif offset2 == offset3:
        if offset3-step >= offsetMinRange[initialStart]:
            offsetMinRange[initialStart] = offset3-step
            offsetMinRange[initialStart+1] = offset3-step
            offsetMinRange[initialStart+2] = offset3-step

        if offset3+step <= offsetMaxRange[initialStart]:
            offsetMaxRange[initialStart] = offset3+step
            offsetMaxRange[initialStart+1] = offset3+step
            offsetMaxRange[initialStart+2] = offset3+step
  return offsetMinRange,offsetMaxRange

def runInitialFrame(initialFrame,NumberOfFrame,step,headingStep,files,TotalScore,offsetMaxRange,offsetMinRange,OffsetArray,HeadingArray):


  for i in range(initialFrame,NumberOfFrame):
      LOGData =  open(LOGPath + str(i) +'.txt', 'r')
      for lines in LOGData:
          if lines[0] == 'r':
              arr = lines.split(':')
              if int(arr[1]) > 0 :
                  offsetMaxRange[i] = (int(arr[1])+100)
              else:
                  offsetMinRange[i] = (int(arr[1])+100)

      for j in range(offsetMinRange[i],offsetMaxRange[i],step):
        OffsetArray[i].append(j)
        print(j)
      for j in range(0,40,headingStep):
        HeadingArray[i].append(j)

  #先放好要排序的順序
  offset1, offset2,offset3,heading1,heading2, heading3 = calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep)
  offsetMinRange,offsetMaxRange = findNextRange(initialFrame,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step)

  
  step = int(step/2)
  OffsetArray= []
  HeadingArray= []
  for i in range(len(files)+450):
     OffsetArray.append([])
     HeadingArray.append([])

  for i in range(initialFrame,NumberOfFrame):
    print('minMax:',offsetMinRange[i],offsetMaxRange[i])
    for j in range(offsetMinRange[i],offsetMaxRange[i]+1,step):
      OffsetArray[i].append(j)
      print(j)
    for j in range(0,40,headingStep):
      HeadingArray[i].append(j)
      #print(j)
  offset1, offset2,offset3,heading1,heading2, heading3 = calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep)
  offsetMinRange,offsetMaxRange = findNextRange(initialFrame,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step)

  print('minMax:',offsetMinRange[i],offsetMaxRange[i])
  step = int(step/2)
  OffsetArray= []
  HeadingArray= []
  for i in range(len(files)+450):
     OffsetArray.append([])
     HeadingArray.append([])

  for i in range(initialFrame,NumberOfFrame):
    for j in range(offsetMinRange[i],offsetMaxRange[i]+1,step):
      OffsetArray[i].append(j)
      print(j)
    for j in range(0,40,headingStep):
      HeadingArray[i].append(j)
      #print(j)
  offset1, offset2,offset3,heading1,heading2, heading3 = calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep)
  offsetMinRange,offsetMaxRange = findNextRange(initialFrame,offset1, offset2,offset3,offsetMinRange,offsetMaxRange,step)

  print('minMax:',offsetMinRange[i],offsetMaxRange[i])
  step = 1
  headingStep = 1
  OffsetArray= []
  HeadingArray= []
  for i in range(len(files)+450):
     OffsetArray.append([])
     HeadingArray.append([])

  for i in range(initialFrame,NumberOfFrame):
    for j in range(offsetMinRange[i],offsetMaxRange[i]+1,step):
      OffsetArray[i].append(j)
      print(j)
    for j in range(0,40,headingStep):
      HeadingArray[i].append(j)
      #print(j)
  offset1, offset2,offset3,heading1,heading2, heading3 = calculateRange(initialFrame,OffsetArray,HeadingArray,step,headingStep)
  return offset1, offset2,offset3,heading1,heading2, heading3



if __name__ == '__main__':
  #TotalScorePath = "D:/lab/ITRI/demo/20190426/demo8/temp/data/finialScore/"
  #LOGPath = "D:/lab/ITRI/demo/20190426/demo8/temp/data/LOG/"
  #fileNamePath = "D:/lab/ITRI/demo/20190426/demo8/temp/data/FileName/"
  TotalScorePath = "F:/straight/temp_0610_circle3/data/finialScore/"
  LOGPath = "F:/straight/temp_0610_circle3/data/LOG/"
  fileNamePath = "F:/straight/temp_0610_circle3/data/FileName/"
  files = listdir(TotalScorePath)
  TotalScore = []
  offsetMaxRange = []
  offsetMinRange = []
  OffsetArray = []
  HeadingArray = []
  for i in range(len(files)+450):
     TotalScore.append([])
     OffsetArray.append([])
     HeadingArray.append([])
     offsetMaxRange.append(-1)
     offsetMinRange.append(-1)
     for j in range(201) :
         TotalScore[i].append([])
         for k in range(41) :
            TotalScore[i][j].append(Score(500))

  finialSortScore = []
  for i in range(len(files)+450):
    finialSortScore.append([])
  frameSmooth = []
  for i in range(len(files)+450):
    frameSmooth.append([])

  
  for f in files:
     ScorePath = open(TotalScorePath + f, 'r')
     #print(f)
     for lines in ScorePath:
         arr= lines.split(',')
         temp = Score(float(arr[3]))
         #temp.fileName = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].fileName
         #print(int(arr[0]),int(arr[1])+100,int(arr[2])+20) 
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20] = temp
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].HogScore = float(arr[6])
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].SegScore = float(arr[4])
  
  files = listdir(fileNamePath)
  for f in files:
    fileNameData =  open(fileNamePath + f, 'r')
    for lines in fileNameData:
        #arr1 = lines.split('/')
        #arr= arr1[5].split('_')
        arr = lines.split('_')
        TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].x = float(arr[3])
        TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].y = float(arr[4])
        TotalScore[int(arr[0]) ][int(arr[1])+100][int(arr[2])+20].heading = float(arr[5])

  initialFrame = 420
  NumberOfFrame = 423
  step = 20
  headingStep = 2
  #offset1, offset2,offset3,heading1,heading2, heading3 = runInitialFrame(initialFrame,NumberOfFrame,step,headingStep,files,TotalScore,offsetMaxRange,offsetMinRange,OffsetArray,HeadingArray)
  #demo5 (16,16,16,21,21,21)
  #固定三個點後延伸

  offset1 = 137
  offset2 = 138
  heading1 = 35
  heading2 = 35


  preOffset = offset1
  preHeading = heading1
  nowOffset = offset2
  nowHeading = heading2

  TotalSmoothScore = 0
  for i in range(initialFrame,419,-1):
      Score = 8000
      finialHeading = 0
      finialOffset = 0

      minRange = -1
      maxRange = -1
      LOGData =  open(LOGPath + str(i) +'.txt', 'r')
      for lines in LOGData:
          if lines[0] == 'r':
              arr = lines.split(':')
              if int(arr[1]) > 0 :
                 maxRange = int(arr[1])+100
              else:
                 minRange =  int(arr[1])+100
      #print(minRange,maxRange)
      for j in range(minRange,maxRange):
          for k in range(0,40):
              temp = calculateSmoothScore(i,preOffset,nowOffset,j,preHeading,nowHeading,k)
              #print(temp)
              if Score > temp:
                  #print(i,j,k,temp,Score)
                  Score = temp
                  finialHeading = k
                  finialOffset = j
      preOffset = nowOffset
      preHeading = nowHeading
      nowOffset = finialOffset
      nowHeading = finialHeading
      TotalSmoothScore += Score
      print(i,finialOffset-100,finialHeading-20,Score)
  print(TotalSmoothScore)

  preOffset = offset1
  preHeading = heading1
  nowOffset = offset2
  nowHeading = heading2
  TotalSmoothScore = 0
  for i in range(initialFrame + 1,419 + len(files)-1):
      Score = 8000
      finialHeading = 0
      finialOffset = 0

      minRange = -1
      maxRange = -1
      LOGData =  open(LOGPath + str(i) +'.txt', 'r')
      for lines in LOGData:
          if lines[0] == 'r':
              arr = lines.split(':')
              if int(arr[1]) > 0 :
                 maxRange = int(arr[1])+100
              else:
                 minRange =  int(arr[1])+100
      #print(minRange,maxRange)
      for j in range(minRange,maxRange):
          for k in range(0,40):
              temp = calculateSmoothScore(i,preOffset,nowOffset,j,preHeading,nowHeading,k)
              #print(temp)
              if Score > temp:
                  #print(i,j,k,temp,Score)
                  Score = temp
                  finialHeading = k
                  finialOffset = j
      if Score != 8000:
        preOffset = nowOffset
        preHeading = nowHeading
        nowOffset = finialOffset
        nowHeading = finialHeading
        TotalSmoothScore += Score
      print(i,finialOffset-100,finialHeading-20,Score)
  print(TotalSmoothScore)

        

