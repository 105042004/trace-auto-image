from unrealcv import client
from unrealcv.automation import UE4Binary
from unrealcv.util import read_png, read_npy
from sklearn import preprocessing
import line
import shutil
import os
import matplotlib.pyplot as plt
import numpy as np
import json
import math

#resultPath = 'D:/lab/ITRI/demo/20190426/demo9/groundTruth/'
resultPath = 'D:/0826/AutoData_demo/20190912_turn_right/temp_after/'

class Camera:
    def __init__(self, client, camera_id):
        self.id = camera_id
        self.client = client

    def set_location(self, loc):
        self.client.request('vset /camera/{id}/location {x} {y} {z}'.format(id = self.id, **loc ))

    def set_rotation(self, rot):
        self.client.request('vset /camera/{id}/rotation {pitch} {yaw} {roll}'.format(id = self.id, **rot))

    def capture_depth(self):
        res = self.client.request('vget /camera/{id}/depth npy'.format(id = self.id))
        depth = read_npy(res)
        return depth

    def capture_img(self,name):
        res = self.client.request('vget /camera/{id}/lit {name}'.format(id = self.id,name = name))
        img = read_png(res)
        return img

def readDatFile():
    ResultData = open(resultPath + "position.dat", 'r')
    count = 0
    location = []
    rotate = []
    for lines in ResultData:
        arr = lines.split(' ')
        location.append(float(arr[1]))
        location.append(float(arr[2]))


    for k in range(0,int(len(location)/2)):
        loc = dict(x = location[2*k], y = location[2*k+1], z = 186.0)
        nextLoc = dict(x = location[2*(k+1)], y = location[2*(k+1)+1], z = 186.0)
        vector1 = (float(loc['x']*100),float(loc['y']*100),0.0)
        vector2 = (float(nextLoc['x']*100),float(nextLoc['y']*100),0.0)
        rot1 = -angle( [vector1[0],vector1[1],vector2[0],vector2[1] ] , [0,0,0,1]) + 90

        rot = dict(pitch = str(0.0), yaw = str(15), roll = str(0.0))
        camera.set_location(loc)
        camera.set_rotation(rot)
        fileName = 'D:/0826/AutoData_demo/20190912_turn_right/image/'+str(count)+'.png'
        print(fileName)
        count = count+ 1
        camera.capture_img(fileName)


def angle(v1, v2):
    dx1 = v1[2] - v1[0]
    dy1 = v1[3] - v1[1]
    dx2 = v2[2] - v2[0]
    dy2 = v2[3] - v2[1]
    angle1 = math.atan2(dy1, dx1)
    angle1 = float(angle1 * 180/math.pi)
    #print(angle1)
    angle2 = math.atan2(dy2, dx2)
    angle2 = float(angle2 * 180/math.pi)
    #print(angle2)
    if angle1*angle2 >= 0:
        included_angle = abs(angle1-angle2)
    else:
        included_angle = abs(angle1) + abs(angle2)
        if included_angle > 180:
            included_angle = 360 - included_angle
    return angle1

def readTXTfileWithNoHeading():
    ResultData = open(resultPath + "position_lineFitting.txt", 'r')
    count = 0

    location = []
    rotate = []
    for lines in ResultData:
        arr = lines.split(',')
        location.append(float(arr[0]))
        location.append(float(arr[1]))

    for k in range(0,int(len(location)/2)):
        loc = dict(x = location[2*k], y = location[2*k+1], z = 186.0)
        nextLoc = dict(x = location[2*(k+1)], y = location[2*(k+1)+1], z = 186.0)
        vector1 = (float(loc['x']*100),float(loc['y']*100),0.0)
        vector2 = (float(nextLoc['x']*100),float(nextLoc['y']*100),0.0)
        rot1 = angle( [vector1[0],vector1[1],vector2[0],vector2[1] ] , [0,0,1,0])

        rot = dict(pitch = str(0.0), yaw = str(rot1), roll = str(0.0))
        print(rot['yaw'])
        camera.set_location(loc)
        camera.set_rotation(rot)
        fileName = resultPath + 'image/'+str(count)+'.png'
        print(fileName)
        count = count+ 1
        camera.capture_img(fileName)

def readTxtFile():
    #ResultData = open(resultPath + "position_lineFitting.txt", 'r')
    ResultData = open(resultPath + "position_Fixed_LS.txt", 'r')
    count = 0
    for lines in ResultData:
        arr = lines.split(',')
        loc = dict(x = arr[0], y = arr[1], z = 186.0)
        rotate = float(arr[2])
        rot = dict(pitch = str(0.0), yaw = str(rotate), roll = str(0.0))
        camera.set_location(loc)
        camera.set_rotation(rot)
        fileName = resultPath + 'image_ori/'+str(count)+'.png'
        print(fileName)
        count = count+ 1
        camera.capture_img(fileName)


def readTxtFilewithHeading():
    ResultData = open(resultPath + "position_lineFitting.txt", 'r')
    #ResultData = open(resultPath + "position_interpolateFitting.txt", 'r')
    count = 0
    locationX = []
    locationY = []
    heading = []
    for lines in ResultData:
        arr = lines.split(',')
        locationX.append(arr[0])
        locationY.append(arr[1])
    HeadingData = open(resultPath + "position_lineFitting_heading.txt", 'r')
    for lines in HeadingData:
        heading.append(lines)
        loc = dict(x = locationX[count], y = locationY[count], z = 186.0)
        rotate = float(lines)
        rot = dict(pitch = str(0.0), yaw = str(rotate), roll = str(0.0))
        camera.set_location(loc)
        camera.set_rotation(rot)
        print(loc,rot)
        fileName = resultPath + 'image/'+str(count)+'.png'
        print(fileName)
        count = count+ 1
        camera.capture_img(fileName)



if __name__ == '__main__':
      camera = Camera(client, 0)
      client.connect()
      #readDatFile()
      #readTXTfileWithNoHeading()
      readTxtFile()
      #readTxtFilewithHeading()
     
      
           
