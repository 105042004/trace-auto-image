import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize
from scipy import interpolate

PositionPath = 'D:/lab/ITRI/demo/20190426/demo3/smoothTermWithFixedData/'



ResultData = open(PositionPath + "position.txt", 'r')
x_coord = []
y = []
heading = []
tempHeading = []
output = ""
count = 0
for lines in ResultData:
    if count>=0 and count <=200:
        #print(lines)
        arr = lines.split(',')
        x_coord.append(float(arr[0]) )
        y.append(float(arr[1]) )
        heading.append(count)
        tempHeading.append(float(arr[2]))
        #print(float(arr[0]))
        #print(float(arr[1]))
    count = count+1
#print(x)
#print(y)




minX =x_coord[0]
maxX = x_coord[len(x_coord)-1]
for i in range(0,len(x_coord)):
    x_coord[i] = minX + i*(maxX - minX)/len(x_coord)
    #print(x_coord[i])


tck,u = interpolate.splprep([x_coord,y],s=10000)
unew = np.arange(0,1.01,0.01)
out = interpolate.splev(unew,tck)
plt.figure()
plt.plot(x_coord,y,out[0],out[1])
plt.show()

output = ''
for i in range(0,len(out[0])):
    output += str(out[0][i])+','+str(out[1][i])+',\n'
    #print(out[0][i],out[1][i])

filenameData= PositionPath +"position_interpolateFitting.txt"
#with open(filenameData, 'w') as file:
#    file.write(output)


for i in range(len(tempHeading)-1):
    if tempHeading[i] * tempHeading[i+1] < 0 and abs(tempHeading[i] - tempHeading[i+1])>=100 :
        if tempHeading[i+1] <0:
            tempHeading[i+1] = 360 + tempHeading[i+1]
        else:
            tempHeading[i+1]  =  tempHeading[i+1] - 360
        print(tempHeading[i+1])



tck,u = interpolate.splprep([heading,tempHeading],s=300)
unew = np.arange(0,1.01,0.01)
out = interpolate.splev(unew,tck)
plt.figure()
plt.plot(heading,tempHeading,out[0],out[1])
plt.show()

output = ''
for i in range(0,len(out[0])):
    output += str(out[1][i])+'\n'
    #print(out[0][i],out[1][i])

filenameData= PositionPath +"position_interpolateFitting_heading.txt"
#with open(filenameData, 'w') as file:
#    file.write(output)

def laplacian(x, a, b, c, d):
    val = a* np.exp(-np.abs(x - b) / c) + d
    return val


"""

filenameData= PositionPath +"position_interpolate.txt"
with open(filenameData, 'w') as file:
    file.write(output)
"""

"""#PositionPath = 'F:/straight/temp/ori/'
for i in range(0,149,120):
    ResultData = open(PositionPath + "position.txt", 'r')
    x_coord = []
    y = []
    heading = []
    tempHeading = []
    output = ""
    count = 0
    print(i)
    for lines in ResultData:
        if count>=i and count <=i+99:
            print(lines)
            arr = lines.split(',')
            x_coord.append(float(arr[1]) )
            y.append(float(arr[0]) )
            heading.append(count)
            tempHeading.append(float(arr[2]))
            #print(float(arr[0]))
            #print(float(arr[1]))
        count = count+1
    #print(x)
    #print(y)




    minX =x_coord[0]
    maxX = x_coord[len(x_coord)-1]
    for i in range(0,len(x_coord)):
        x_coord[i] = minX + i*(maxX - minX)/len(x_coord)
        #print(x_coord[i])




    # define a laplacian function to fit the data
    def laplacian(x, a, b, c, d):
        val = a* np.exp(-np.abs(x - b) / c) + d
        return val

    # fit the data    
    popt, pcov = optimize.curve_fit(laplacian, x_coord, y, p0=[x_coord[0],y[0],x_coord[len(x_coord)-1],y[len(x_coord)-1]])

    # plot the data and the fitting curve
    plt.plot(x_coord, y, 'b-', x_coord, laplacian(x_coord, *popt), 'r:')
    y_result = laplacian(x_coord, *popt)
    output = ''
    #print(len(x_coord),len(y_result))
    for i in range(0,len(x_coord)):
        output += str(y_result[i])+','+ str(x_coord[i])+',\n'
        #print(x_coord[i],y_result[i])
    plt.show()

    filenameData= PositionPath +"position_curveFitting.txt"
    with open(filenameData, 'a') as file:
        file.write(output)

    #print(heading,tempHeading)
    # fit the data    
    popt, pcov = optimize.curve_fit(laplacian, heading, tempHeading, p0=[heading[0],tempHeading[0],heading[len(x_coord)-1],tempHeading[len(x_coord)-1]])

    # plot the data and the fitting curve
    plt.plot(heading, tempHeading, 'b-', heading,laplacian(heading, *popt), 'r:')
    y_result = laplacian(heading, *popt)
    output = ''
    #print(len(heading),len(y_result))
    for i in range(0,len(heading)):
        output += str(y_result[i])+'\n'
        #print(heading[i],y_result[i])
    plt.show()



    filenameData= PositionPath +"position_curveFitting_heading.txt"
    with open(filenameData, 'a') as file:
        file.write(output)
    """