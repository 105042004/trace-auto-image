import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join
maxRange = 200
minRange = 0

class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1
  self.fileName = ""
  self.HogScore = 5000
  self.SegScore = -1


def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealHOGScore(x,maxNumber):
    return (1+1*x/maxNumber)/2


def method1(TotalScore,i):
    minScore = 100000
    minOffset = -1
    minHeading = -1
    for j in range(0,len(TotalScore[i])):
        for k in range(0,len(TotalScore[i][j])):
            if TotalScore[i][j][k].score < minScore and j>=minRange and j<=maxRange:
                minScore = TotalScore[i][j][k].score
                minOffset = j
                minHeading = k
    #print(i,minOffset-100,minHeading-20,minScore)
    return minOffset,minHeading,minScore
    #print(TotalScore[i][minOffset][minHeading].fileName)



def method2(TotalScore,i):
    minScore = 100000
    minOffset = -1
    minHeading = -1
    minValueIndex = -1
    for j in range(0,len(TotalScore[i])):
        if TotalScore[i][j][20].score < minScore and j>= minRange and j<= maxRange:
                minScore = TotalScore[i][j][20].score
                minOffset = j
                minHeading = 20
    minValueIndex = minOffset

    minScore = 10000
    minHeading = -1
    for j in range(0,len(TotalScore[i][minValueIndex])):
            if TotalScore[i][minValueIndex][j].score < minScore:
                minScore = TotalScore[i][minValueIndex][j].score
                minHeading = j
    #print(i,minValueIndex-100,minHeading-20,minScore) 
    return minValueIndex,minHeading,minScore



def method3(TotalScore):
    minValueIndex = []
    for i in range(0,len(TotalScore)):
      minScore = 100000
      minOffset = -1
      minHeading = -1
      for j in range(74,200):
          if TotalScore[i][j][20].score < minScore:
                  minScore = TotalScore[i][j][20].score
                  minOffset = j
                  minHeading = 20
      minValueIndex.append(minOffset)

    for i in range(0,len(minValueIndex)):
        minScore = 10000
        minHeading = -1
        for j in range(0,len(TotalScore[i][minValueIndex[i]])):
             if TotalScore[i][minValueIndex[i]][j].score < minScore:
                  minScore = TotalScore[i][minValueIndex[i]][j].score
                  minHeading = j
        print(i,minValueIndex[i]-100,minHeading-20,minScore) 
    """
        if i == 6 or i ==7 :
           floorPhoto = cv2.imread(floorPath2+TotalScore[i][minValueIndex[i]][20].fileName)
        else:
           floorPhoto = cv2.imread(floorPath+TotalScore[i][minValueIndex[i]][20].fileName)  
             
        print(i,minValueIndex[i]-100,minHeading-20,minScore) 
        finalPhoto = cv2.imread(photoPath+str(i)+'.png')
        finalPhoto = cv2.resize(finalPhoto,(1280,720))
        img=cv2.addWeighted(finalPhoto,0.3,floorPhoto,0.7,0)
        cv2.imwrite('F:/straight/3_result_3/'+str(i)+'.png',img)
        """



def method4(TotalScore):
    for i in range(0,len(TotalScore)):
      minScore = 100000
      minOffset = -1
      minHeading = -1
      for j in range(74,200):
          for k in range(0,len(TotalScore[i][j])):
              if TotalScore[i][j][k].score < minScore:
                  minScore = TotalScore[i][j][k].score
                  minOffset = j
                  minHeading = k
      print(i,minOffset-100,minHeading-20,minScore)
      print(TotalScore[i][minOffset][minHeading].fileName)
      if i == 6 or i ==7 :
          floorPhoto = cv2.imread(floorPath2+TotalScore[i][minOffset][minHeading].fileName)
      else:
          floorPhoto = cv2.imread(floorPath+TotalScore[i][minOffset][minHeading].fileName)      
      finalPhoto = cv2.imread(photoPath+str(i)+'.png')
      finalPhoto = cv2.resize(finalPhoto,(1280,720))
      img=cv2.addWeighted(finalPhoto,0.3,floorPhoto,0.7,0)

      #cv2.imshow('Canny2', img )
      cv2.imwrite('F:/straight/4_result/'+str(i)+'.png',img)
      #cv2.waitKey(0)

def method5(TotalScore,i):

    minValueIndex = -1
    
   
    allHeadingOffset = []
    minValueCount = []
    for o in range(201):
        allHeadingOffset.append(0)
        minValueCount.append(0)

    #print(i)
    for j in range(0,200):
        for k in range(0,len(TotalScore[i][j])):
            if TotalScore[i][j][k].score < 1 and j>= minRange and j<=maxRange:
                #print(TotalScore[i][j][k].score)
                allHeadingOffset[j] += TotalScore[i][j][k].score
                minValueCount[j] = minValueCount[j]+1

    for j in range(0,len(allHeadingOffset)):
        if(allHeadingOffset[j] == 0):
            allHeadingOffset[j] = 10000
        else:
            allHeadingOffset[j] = allHeadingOffset[j]/minValueCount[j]
            #print(j-100,allHeadingOffset[j])

    minValueIndex = allHeadingOffset.index(min(allHeadingOffset))


    
    minScore = 10000
    minHeading = -1
    for j in range(0,len(TotalScore[i][minValueIndex])):
        if TotalScore[i][minValueIndex][j].score < minScore:
            minScore = TotalScore[i][minValueIndex][j].score
            minHeading = j
    
    #print(i,minValueIndex-100,minHeading-20,minScore) 
    return minValueIndex,minHeading,minScore




def method6(TotalScore):



    for i in range(0,len(TotalScore)):
       
        if i == 6 or i ==7 :
            floorPhoto = cv2.imread(floorPath2+TotalScore[i][100][20].fileName)
        else:
            floorPhoto = cv2.imread(floorPath+TotalScore[i][100][20].fileName)     
            finalPhoto = cv2.imread(photoPath+str(i)+'.png')
            finalPhoto = cv2.resize(finalPhoto,(1280,720))
            img=cv2.addWeighted(finalPhoto,0.3,floorPhoto,0.7,0)
            cv2.imwrite('F:/straight/ori/'+str(i)+'.png',img)

        #print(allHeadingOffset.index(min(allHeadingOffset))-100)
def method7(TotalScore):
    for i in range(0,len(TotalScore)):
      minScore = 100000
      minOffset = -1
      minHeading = -1
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              if TotalScore[i][j][k].HogScore < minScore and j >= minRange and j <= maxRange:
                  minScore = TotalScore[i][j][k].HogScore
                  minOffset = j
                  minHeading = k
      print(i,minOffset-100,minHeading-20,minScore)
      #print(TotalScore[i][minOffset][minHeading].fileName)

def method8(TotalScore):
    for i in range(0,len(TotalScore)):
      minScore = 0.0000001
      minOffset = -1
      minHeading = -1
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              if TotalScore[i][j][k].SegScore > minScore  and j >= minRange and j <= maxRange:
                  minScore = TotalScore[i][j][k].SegScore
                  minOffset = j
                  minHeading = k
      print(i,minOffset-100,minHeading-20,minScore)
      #print(TotalScore[i][minOffset][minHeading].fileName)


def method9(TotalScore,i):
    minValueIndex = -1

    minScore = 5000
    minOffset = -1
    minHeading = -1
    for j in range(0,len(TotalScore[i])):
        if TotalScore[i][j][20].HogScore < minScore and  j >= minRange and j <= maxRange:
                minScore = TotalScore[i][j][20].HogScore
                minOffset = j
                minHeading = 20
    minValueIndex = minOffset

    
    minScore = 5000
    minHeading = -1
    for j in range(0,len(TotalScore[i][minValueIndex ])):
            if TotalScore[i][minValueIndex ][j].HogScore < minScore:
                minScore = TotalScore[i][minValueIndex ][j].HogScore
                minHeading = j
    #print(i,minValueIndex-100,minHeading-20,minScore) 
    return minValueIndex,minHeading,minScore


def method10(TotalScore,i):
    minValueIndex = -1
    minScore = -1
    minOffset = -1
    minHeading = -1
    for j in range(0,len(TotalScore[i])):
        if TotalScore[i][j][20].SegScore > minScore and j >= minRange and j<= maxRange:
                minScore = TotalScore[i][j][20].SegScore
                minOffset = j
                minHeading = 20
    minValueIndex = minOffset

    
    minScore = -1
    minHeading = -1
    for j in range(0,len(TotalScore[i][minValueIndex])):
            if TotalScore[i][minValueIndex][j].SegScore > minScore:
                minScore = TotalScore[i][minValueIndex][j].SegScore
                minHeading = j
    #print(i,minValueIndex-100,minHeading-20,minScore) 
    return minValueIndex,minHeading,minScore

def method11(TotalScore):

    MaxHog = []
    for i in range(201):
      MaxHog.append(-1)

    MaxSeg = []
    for i in range(201):
      MaxSeg.append(-1)

    for i in range(0,len(TotalScore)):
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              if TotalScore[i][j][k].SegScore>MaxSeg[i] and TotalScore[i][j][k].SegScore != -1 :
                    MaxSeg[i] = TotalScore[i][j][k].SegScore
              if TotalScore[i][j][k].HogScore>MaxHog[i] and TotalScore[i][j][k].HogScore != 5000:
                    MaxHog[i] = TotalScore[i][j][k].HogScore


    for i in range(0,len(TotalScore)):
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              score1 = dealHOGScore(TotalScore[i][j][k].HogScore /10,MaxHog[i]/10)
              score2 = sigmoid(10*TotalScore[i][j][k].SegScore,(1+10*MaxSeg[i]))

              weight = 0.4
              TotalScore[i][j][k].score = weight*score2 +(1-weight)*score1

    for i in range(0,len(TotalScore)):
      minScore = 100000
      minOffset = -1
      minHeading = -1
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              if TotalScore[i][j][k].score < minScore:
                  minScore = TotalScore[i][j][k].score
                  minOffset = j
                  minHeading = k
      print(i,minOffset-100,minHeading-20,minScore)
      #print(TotalScore[i][minOffset][minHeading].fileName)

def method12(TotalScore):

    MaxHog = []
    for i in range(200):
      MaxHog.append(-1)

    MaxSeg = []
    for i in range(200):
      MaxSeg.append(-1)

    for i in range(0,len(TotalScore)):
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              if TotalScore[i][j][k].SegScore>MaxSeg[i] and TotalScore[i][j][k].SegScore != -1 :
                    MaxSeg[i] = TotalScore[i][j][k].SegScore
              if TotalScore[i][j][k].HogScore>MaxHog[i] and TotalScore[i][j][k].HogScore != 5000:
                    MaxHog[i] = TotalScore[i][j][k].HogScore


    for i in range(0,len(TotalScore)):
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              score1 = -((sigmoid(TotalScore[i][j][k].HogScore/10,1+MaxHog[i]/10))-1)
              score2 = 1-dealHOGScore(10*TotalScore[i][j][k].SegScore,(10*MaxSeg[i]))

              weight = 0.8
              TotalScore[i][j][k].score = weight*score2 +(1-weight)*score1

    for i in range(0,len(TotalScore)):
      minScore = 100000
      minOffset = -1
      minHeading = -1
      for j in range(0,len(TotalScore[i])):
          for k in range(0,len(TotalScore[i][j])):
              if TotalScore[i][j][k].score < minScore  and j >= minRange and j <= maxRange:
                  minScore = TotalScore[i][j][k].score
                  minOffset = j
                  minHeading = k
      print(i,minOffset-100,minHeading-20,minScore)
      #print(TotalScore[i][minOffset][minHeading].fileName)
    
#先算heading總和
def method13(TotalScore,i):
    minValueIndex =-1
    

    allHeadingOffset = []
    minValueCount = []
    for o in range(41):
        allHeadingOffset.append(0)
        minValueCount.append(0)

    #print(i)
    for j in range(0,len(TotalScore[i])):
        for k in range(0,len(TotalScore[i][j])):
            if TotalScore[i][j][k].score < 1 and j>= minRange and j<= maxRange:
                #print(TotalScore[i][j][k].score)
                allHeadingOffset[k] += TotalScore[i][j][k].score
                minValueCount[k] = minValueCount[k]+1

    for j in range(0,len(allHeadingOffset)):
        if(allHeadingOffset[j] == 0):
            allHeadingOffset[j] = 10000
        else:
            allHeadingOffset[j] = allHeadingOffset[j]/minValueCount[j]
            #print(j-100,allHeadingOffset[j])

    minValueIndex = allHeadingOffset.index(min(allHeadingOffset))



    minScore = 10000
    minOff = -1
    for j in range(0,len(TotalScore[i])):
        if TotalScore[i][j][minValueIndex].score < minScore and j>= minRange and j<= maxRange:
            minScore = TotalScore[i][j][minValueIndex].score
            minOff = j
    minScore = 10000
    minHeading = -1
    for k in range(0,len(TotalScore[i][minOff])):
        if TotalScore[i][minOff][k].score < minScore and j>= minRange and j<= maxRange:
                minScore = TotalScore[i][minOff][k].score
                minHeading = k
    #print(minOff,minHeading,minScore)
    return minOff,minHeading,minScore

#把周圍的候選都選出來
def method14(TotalScore):

    for i in range(0,len(TotalScore)):
      print(i)
      for o in range(0,20):
        minScore = 100000
        minOffset = -1
        minHeading = -1
        for j in range(0,len(TotalScore[i])):
            for k in range(0,len(TotalScore[i][j])):
                if TotalScore[i][j][k].score < minScore:
                    minScore = TotalScore[i][j][k].score
                    minOffset = j
                    minHeading = k

        for j in range(0,len(TotalScore[i][minOffset])):
            TotalScore[i][minOffset][j].score = 5000
        print(i,minOffset-100,minHeading-20,minScore)
    """
    for i in range(0,len(TotalScore)):
        print(i)
        for o in range(0,20):
          minValueIndex = []
          minScore = 100000
          minOffset = -1
          for j in range(0,len(TotalScore[i])):
            if TotalScore[i][j][24].score < minScore and j>=100:
                  minScore = TotalScore[i][j][24].score
                  minOffset = j
                  minHeading = 20
          TotalScore[i][minOffset][24].score = 5000
          print(i,minOffset-100,minHeading-20,minScore)
          minValueIndex.append(minOffset)
          """

def seg(TotalScore,i):
    minScore = -1
    minOffset = -1
    minHeading = -1
    for j in range(0,len(TotalScore[i])):
        for k in range(0,len(TotalScore[i][j])):
            if TotalScore[i][j][k].SegScore > minScore and j>=minRange and j<=maxRange:
                minScore = TotalScore[i][j][k].SegScore
                minOffset = j
                minHeading = k
    #print(i,minOffset-100,minHeading-20,minScore)
    return minOffset,minHeading,minScore
    #print(TotalScore[i][minOffset][minHeading].fileName)

def hog(TotalScore,i):
    minScore = 50000
    minOffset = -1
    minHeading = -1
    for j in range(0,len(TotalScore[i])):
        for k in range(0,len(TotalScore[i][j])):
            if TotalScore[i][j][k].HogScore < minScore and j>=minRange and j<=maxRange:
                minScore = TotalScore[i][j][k].HogScore
                minOffset = j
                minHeading = k
    #print(i,minOffset-100,minHeading-20,minScore)
    return minOffset,minHeading,minScore
    #print(TotalScore[i][minOffset][minHeading].fileName)


if __name__ == '__main__':
  TotalScorePath = "D:/lab/ITRI/demo/20190426/demo8/temp/data/finialScore/"
  groundTruthPath = 'D:/lab/ITRI/demo/20190426/demo8/groundTruth/result.txt'
  #floorPath = "F:/straight/Merge/"
  #floorPath2 = "F:/straight/Merge2/"
  
  TotalScore = []
  groundTruth = [] 
  groundTruthHeading = []
  for i in range(550):
     TotalScore.append([])
     groundTruth.append([])
     groundTruthHeading.append([])
     for j in range(201) :
         TotalScore[i].append([])
         for k in range(41) :
            TotalScore[i][j].append(Score(500))

   
  groundTruthData = open(groundTruthPath,'r')
  for lines in groundTruthData:
      arr= lines.split(' ')
      groundTruth[int(arr[0])] = int(arr[1])
      groundTruthHeading[int(arr[0])] = int(arr[2])




  files = listdir(TotalScorePath)
  for f in files:
     ScorePath = open(TotalScorePath + f, 'r')
     for lines in ScorePath:
         arr= lines.split(',')
         temp = Score(float(arr[3]))
         #temp.fileName = TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].fileName
         #print(int(arr[0]),int(arr[1])+100,int(arr[2])+20) 
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20] = temp
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].HogScore = float(arr[6])
         TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].SegScore = float(arr[4])
         #print(TotalScore[int(arr[0])][int(arr[1])+100][int(arr[2])+20].score,float(arr[6]),float(arr[4]) 
  count = 0
  value = 0
  for i in range(0,100):
      #print(groundTruth[i])
      for o in range(0,10000):
        minOffset,Heading,Score = method1(TotalScore,i)
        if  abs(minOffset-100 - groundTruth[i]) <=2 and abs(Heading-20 - groundTruthHeading[i]) <= 1:
            print(i,o/8000,o,minOffset-100,Heading-20,Score)
            count = count +1
            value += o
            break 
        if o == 9999:
            print(i,'','',groundTruth[i],groundTruthHeading[i])
            break
        if minOffset == -1 and Heading == -1 :
            print(i,'','',groundTruth[i],groundTruthHeading[i])
            break

        
        TotalScore[i][minOffset][Heading].score = 5000
        TotalScore[i][minOffset][Heading].SegScore = -1
        TotalScore[i][minOffset][Heading].HogScore = 5000
        
        """
        for j in range(0,len(TotalScore[i][minOffset])):
            TotalScore[i][minOffset][j].score = 5000
            TotalScore[i][minOffset][j].SegScore = -1
            TotalScore[i][minOffset][j].HogScore = 5000
        """
        
  print(count,value,value/count)
"""
  files = listdir(floorPath)
  for f in files:
      arr = f.split('_')
      if int(arr[0]) <=120 :
          TotalScore[int(arr[0])][int(arr[1])+100][int(arr[4])+20].fileName = f
          #print(TotalScore[int(arr[0])][int(arr[1])+100][int(arr[4])+20].fileName)

  files = listdir(floorPath2)
  for f in files:
      arr = f.split('_')
      if int(arr[0]) <=120 :
        #print(int(arr[0]),int(arr[1])+100,int(arr[4])+20)
        TotalScore[int(arr[0])][int(arr[1])+100][int(arr[4])+20].fileName = f
"""



"""
      for i in range(0,720):
          for j in range(0,1280):
              if floorPhoto[i][j][0] == 0 :
                  floorPhoto[i][j][0] = (floorPhoto[i][j][0] + finalPhoto[i][j][0])/2
                  floorPhoto[i][j][1] = (floorPhoto[i][j][1] + finalPhoto[i][j][1])/2
                  floorPhoto[i][j][2] = (floorPhoto[i][j][2] + finalPhoto[i][j][2])/2
"""




              
          