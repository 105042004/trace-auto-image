import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

class Score():
 def __init__(self,score):
  self.score = score
  self.finalScore = -1
  self.finalTotalScore = -1

def sigmoid(x,maxNumber):
   return  1-(1 / (1 + 50*np.exp(-10*x/maxNumber)))

def dealHOGScore(x,maxNumber):
    return (1+1*x/maxNumber)/2

def run(SegDataPath,HOGDataPath,outputDataPath,index,offsetIndex):
  HOGScore = []
  SegScore = []
  print('finalScore')
  for i in range(0,1):
     HOGScore.append([])
     SegScore.append([])
     for j in range(201) :
         HOGScore[i].append([])
         SegScore[i].append([])
         for k in range(41) :
            HOGScore[i][j].append(Score(500))
            SegScore[i][j].append(Score(500))
     
  MaxHog = -1
  MinHog = -1
  MaxSeg = -1
  MinHog = -1

  HOGPath = open(HOGDataPath + str(index)+".txt", 'r')
  for lines in HOGPath:
    print('HOGLINE')
    lines = lines.strip() #去除首尾空格
    if not len(lines) or lines.startswith('#'):       #判断是否是空行或注释行  
       continue
    arr = lines.split(',')
    #print(int(arr[0]),int(arr[1].split('.')[0])+100,arr[2],arr[3])
    temp = Score(float(arr[3]))            # arr[3] = HOG score!!!!!!!!!!!!!
    HOGScore[0][int(arr[1])+100][int(arr[2])+20] = temp
    #print(int(arr[0]),int(arr2[0])+100,int(arr[2])+20)
    #print(HOGScore[0][0][0].score)
    if(float(arr[3]) > MaxHog):
        MaxHog = float(arr[3]) #book maxHog score
      


  SegPath = open(SegDataPath + str(index)+".txt", 'r')
  for lines in SegPath:
      print('SEGLINE')
      lines = lines.strip()
      if not len(lines) or lines.startswith('#'):       #判断是否是空行或注释行  
        continue
      arr = lines.split(',')
          #print(arr[0])
      temp = Score(float(arr[5]))         ##arr[5] = SOG pixel/allPixel score!!!!!!!!!!!1
          #print(str(int(arr[0]) ) + ',' +str(int(arr[1])+100) +','+ str(int(arr[2])+20))
      SegScore[0][int(arr[1])+100][int(arr[2])+20] = temp 
      if(float(arr[5]) > MaxSeg):
          MaxSeg = float(arr[5]) #book maxSeg Score
          
  for i in range(0,1):
      
      for j in range(offsetIndex+100,offsetIndex+101):
          for k in range(0,len(HOGScore[i][j])):
              HOGScore[i][j][k].finalScore = dealHOGScore(HOGScore[i][j][k].score/10,MaxHog/10)
              SegScore[i][j][k].finalScore = sigmoid(10*SegScore[i][j][k].score,(1+10*MaxSeg))
              weight = 0.8
              SegScore[i][j][k].finalTotalScore = weight*SegScore[i][j][k].finalScore+(1-weight)*HOGScore[i][j][k].finalScore
              #print(str(i),str(j-100),str(k-20),SegScore[i][j][k].finalTotalScore,SegScore[i][j][k].score,HOGScore[i][j][k].score)
              #print(str(SegScore[i][j][k].finalTotalScore)+','+str(SegScore[i][j][k].finalScore)+','+str(HOGScore[i][j][k].finalScore) )
              outputPhotoData = str(index)+","+str(j-100) + ","+str(k-20)+", " +str(SegScore[i][j][k].finalTotalScore) +","+str(SegScore[i][j][k].score)+","+ str(SegScore[i][j][k].finalScore) +","+str(HOGScore[i][j][k].score) +","+str(HOGScore[i][j][k].finalScore)+"\n"
              print(outputPhotoData)
              print('calculateScore')
              print(HOGScore[i][j][k].score)
              if HOGScore[i][j][k].score != 500:
                filenameData=outputDataPath+str(index)+".txt"
                with open(filenameData, 'a') as file:
                  file.write(outputPhotoData)
