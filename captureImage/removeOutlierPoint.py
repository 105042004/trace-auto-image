import shutil
import os
import matplotlib.pyplot as plt
import numpy as np
import json
import math

resultPath = 'C:/Users/Tina/Documents/Unreal Projects/captureImage/ResultData/method1/'


if __name__ == '__main__':
  location = []
  HOGPath = open(resultPath + "position.txt", 'r')
  for lines in HOGPath:
      arr = lines.split(',')
      location.append(float(arr[0]))
      location.append(float(arr[1]))
 
  count  = 0
  output = ''
  outputDat = ''
  for i in range(0,int(len(location)/2)-1):
      if location[i*2] > location[2*(i+1)]:
          location[i*2] = (location[(i-1)*2] + location[(i+1)*2])/2
      output += str(location[i*2])+','+str(location[i*2+1]) +',' +str(0)+'\n'
      outputDat += str(count) + ' '+str(0.43785) + ' '+str(location[i*2])+' '+str(location[i*2+1])+' '+str(count*100)+'\n'
      count = count +1
      print(location[i*2])

  filenameData= resultPath +"position_2.txt"
  with open(filenameData, 'w') as file:
    file.write(output)
  filenameData= resultPath +"position_2.dat"
  with open(filenameData, 'w') as file:
    file.write(outputDat)
