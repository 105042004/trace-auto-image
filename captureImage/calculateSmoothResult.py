import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join

class frameDis():
     def __init__(self,offset,heading):
       self.offset = offset
       self.heading = heading

if __name__ == '__main__':
    finialOffset = []
    frameSmooth = []
    for i in range(300):
       frameSmooth.append([])
       finialOffset.append(frameDis(-1,-1))

    path = 'D:/lab/ITRI/demo/20190426/demo5/method1/smooth_result2.txt'
    filePath = open(path, 'r')
    for lines in filePath:
        arr = lines.split(' ')
        frameSmooth[int(arr[0])-1].append(frameDis(int(arr[3]),int(arr[6])))
        frameSmooth[int(arr[0])].append(frameDis(int(arr[1]),int(arr[4])))
        frameSmooth[int(arr[0])+1].append(frameDis(int(arr[2]),int(arr[5])))
    frameSmooth[0].append(frameSmooth[0][0])
    frameSmooth[0].append(frameSmooth[0][0])
    frameSmooth[1].append(frameSmooth[1][0])
    for i in range(0,len(frameSmooth)):
        if frameSmooth[i][0].offset == frameSmooth[i][1].offset:
            finialOffset[i].offset = frameSmooth[i][0].offset
        elif frameSmooth[i][0].offset == frameSmooth[i][2].offset:
            finialOffset[i].offset = frameSmooth[i][0].offset
        elif frameSmooth[i][1].offset == frameSmooth[i][2].offset:
            finialOffset[i].offset = frameSmooth[i][1].offset
        else:
            finialOffset[i].offset = int((frameSmooth[i][0].offset + frameSmooth[i][1].offset + frameSmooth[i][2].offset)/3)

        if frameSmooth[i][0].heading == frameSmooth[i][1].heading:
            finialOffset[i].heading = frameSmooth[i][0].heading
        elif frameSmooth[i][0].heading == frameSmooth[i][2].heading:
            finialOffset[i].heading = frameSmooth[i][0].heading
        elif frameSmooth[i][1].heading == frameSmooth[i][2].heading:
            finialOffset[i].heading = frameSmooth[i][1].heading
        else:
            finialOffset[i].heading = int((frameSmooth[i][0].heading + frameSmooth[i][1].heading + frameSmooth[i][2].heading)/3)

        print(i,finialOffset[i].offset,finialOffset[i].heading)
