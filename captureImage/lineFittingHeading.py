import matplotlib.pyplot as plt
import numpy as np 
PositionPath = 'D:/lab/ITRI/demo/20190426/demo11/smoothTermTest/'
#PositionPath = 'F:/straight/temp/ori/'
ResultData = open(PositionPath + "position.txt", 'r')
x = []
y = []
heading = []
tempHeading = []
output = ""
count = 0
for lines in ResultData:
    if count>=0 and count <=200:
        #print(lines)
        arr = lines.split(',')
        if float(arr[2]) <0:
            y.append(float(arr[2]) )
        else:
            y.append(float(arr[2]) )
        x.append(count)
        heading.append(float(arr[2]))
        tempHeading.append(float(arr[2]))
        #print(float(arr[0]))
        #print(float(arr[1]))
    count = count+1
#print(x)
#print(y)



z = np.polyfit(x, y, 3)
p = np.poly1d(z)
p30 = np.poly1d(np.polyfit(x, y, 3))
xp = np.linspace(min(x),max(x) , 100)

"""
minX = x[0]
maxX = x[len(x)-1]
for i in range(0,len(x)):
    x[i] = minX + i*(maxX - minX)/len(x)
    print(x[i])
"""
for j in range(10):
    for i in range(0,len(heading)-1):
        if abs(heading[i+1] - heading[i]) <= 1 :
            heading[i] = (heading[i+1] + heading[i])/2
            print(heading[i])
        else:
            if abs(heading[i-1] - heading[i-1]) <= 1 :
                heading[i] = heading[i-1]
                heading[i] = (heading[i+1] + heading[i])/2
                print(heading[i])
            elif  abs(heading[i+2] - heading[i-2]) <= 1 :
                heading[i] = heading[i-2]
                heading[i] = (heading[i+1] + heading[i])/2
                print(heading[i])

"""
for i in range(0,len(heading)):
    heading[i] = min(tempHeading)
    tempHeading[tempHeading.index(min(tempHeading))] = 1000
    print(heading[i])
"""


for i in range(0,len(x)):
    #print('ori: ',x[i],y[i])
    output += str(p(x)[i])+'\n'
    #output += str(p(x)[i])+','+str(x[i])+'\n'
    #output += str(p(x)[i])+','+str(x[i])+','+str(heading[i])+'\n'
    yResult = p(x[i])
    #print('result: ',x[i],p(x)[i])
_ = plt.plot(x, y, '.', xp, p(xp), '-', xp, p30(xp), '--')
filenameData= PositionPath +"position_lineFitting_heading.txt"
with open(filenameData, 'a') as file:
    file.write(output)
plt.ylim(min(y)-10,min(y)+max(x)-min(x)+10)
plt.ylim(min(y)-100,min(y)+100)
plt.xlim(min(x)-100,max(x)+100)
plt.show()