import cv2
import glob
import re
import numpy as np 
from os import listdir
from os.path import isfile, join 

minValue = 99999
filename = ""
outputData = ""
outputPhotoData = ""
token = ""
_index = 0


def is_imag(filename):
    global _index
    #match_object = re.match(token, filename)
    token = str(_index)+"."
    #print(token)
    match_object = re.match(token, filename)
    if match_object:
        #print(match_object.group())
        return True
    else:
        #print(match_object)
        return False

def getDirFile(definePath,comparePath,index):

    myFiles = str(index) + '.png_line.png'
            
    image = cv2.imread(definePath+myFiles, 1)
    print(definePath+myFiles)
    #cv2.imshow('Canny', image )
    arr = myFiles.split('.')
    image2=cv2.resize(image,(512,256))
    img = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
    image3 = cv2.imread(comparePath + str(arr[0])+".png", 1)
    print('HOG '+comparePath + str(arr[0])+".png")
    print('HOG '+definePath+myFiles,_index)
    image4 = cv2.resize(image3,(512,256))
    img2 = cv2.cvtColor(image4, cv2.COLOR_BGR2GRAY)
    feature = []
    feature2 = []
    locations = []
    locations2 = []
    #winSize = cvSize(128, 64)
    #blockSize = cv2.Size(16,16)
    #strideSize = cv2.Size(8,8)
    #cellSize = cv2.Size(8,8)
    bins = 9
    hog = cv2.HOGDescriptor((512,256), (16,16), (8,8), (8,8), bins)
    hog2 = cv2.HOGDescriptor((512,256), (16,16), (8,8), (8,8), bins)
    feature = hog.compute(img,(8, 8), (0, 0), locations)
    feature2 = hog.compute(img2,(8, 8), (0, 0), locations2)
    Hogfeat = np.array(feature ,np.float32)
    for j in range(0,len(feature)):
        Hogfeat[j][0] = feature[j]
    
    Hogfeat2 = np.array(feature2 ,np.float32)
    for j in range(0,len(feature2)):
        Hogfeat2[j][0] = feature2[j]

    distance = 0
    for j in range(0,len(Hogfeat)):
        distance += abs(Hogfeat[j][0] - Hogfeat2[j][0])
        
    if distance >=0 :
        arr = myFiles.split('.')
        totalString = arr[0]+','+str(distance) + "\n"
        #distanceArray.append((myFiles[i],k,distance))
        #print(distanceArray[0])
        global outputPhotoData
        print(totalString)  
        outputPhotoData += totalString 
    #img3=cv2.addWeighted(img,0.3,img2,0.7,0) 
    #cv2.imwrite('F:/straight/temp/data/test/1_'+str(distance)+'_'+myFiles[i],image)
    #cv2.imwrite('F:/straight/temp/data/test/2_'+str(distance)+'_'+myFiles[i],img2)
    #cv2.imshow('Canny2', img2 )
    #cv2.waitKey(0)

def calculateHOG(definePath,comparePath,outputPath,index):
    global outputPhotoData
    global _index
    outputPhotoData = ""
    
    _index = index
    getDirFile(definePath,comparePath,index)


    filenameData=outputPath+str(index)+".txt"
    with open(filenameData, 'a') as file:
        file.write(outputPhotoData)
    

        