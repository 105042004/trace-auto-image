from unrealcv import client
from unrealcv.automation import UE4Binary
from unrealcv.util import read_png, read_npy
from sklearn import preprocessing
import cv2
import numpy as np 
import line
import shutil
import os
import CalculateHog
import CalculateFloorRatioVideo
import CalculateScoreVideo
# If you see errors of unrealcv.automation and util, use pip install --upgrade unrealcv

import matplotlib.pyplot as plt
import numpy as np
import json
import math

#outputDataPath = 'E:/0826/AutoData/temp/'
#inputDataPath = 'E:/0826/AutoData/'
outputDataPath = 'D:/AutoImage/data/model_filled_afterUnreal/temp/'
inputDataPath = 'D:/AutoImage/data/model_filled_afterUnreal/'
class PlaybackSequence:
    def __init__(self, json_filename):
        record = json.load(open(json_filename))
        self.record = record
        self.cam_info = record['path']

    def get(self, frameid):
        '''
        Returns the vectors of current camera position and move rotation and move vector

        parameter:
            frameid: the frame number
        output:
            vector: dict(x, y, z) of desired frame, fits scale of unreal
            rot1: dict(pitch: , yaw: angle bwetween two frames, roll: )
            moveVector: not really sure

        '''
        frame = self.cam_info[frameid]
        loc = dict(x = frame['lat'], y = frame['lon'], z = 0.0) #'lat'緯度, 'lon'經度
        #loc example : {'x': 111.66808939071701, 'y': 10.121914824293032, 'z': 0.0}

        if frameid + 3 < len(self.cam_info):
            nextFrameId = frameid + 3
        else:
            nextFrameId =  len(self.cam_info)-1

        nextFrame = self.cam_info[nextFrameId]
        nextLoc = dict(x = nextFrame['lat'], y = nextFrame['lon'], z = 0.0)

        #location should be multiplied by 100 to fit the scale in Unreal.
        vector1 = (float(loc['x']*100), float(loc['y']*100), 0.0)
        vector2 = (float(nextLoc['x']*100), float(nextLoc['y']*100), 0.0)
        # print(vector1,vector1)

        rot = angle([vector1[0], vector1[1], vector2[0], vector2[1]], [0,0,1,0]) #rot(rotation): 兩frame之間的夾角
        #print(vector1)
        # print(rot)
        # print('------------')

        vector = dict( x= str(vector1[0]), y= str(vector1[1]), z= str(186))
        rot1 = dict( pitch= str(0.0), yaw= str(rot), roll= str(0.0))
        move = (vector2[0]-vector1[0], vector2[1]-vector1[1], 0)
        moveVector = [-move[1],move[0],0]
        moveVector = moveVector/np.linalg.norm(moveVector)

        return vector, rot1, moveVector

    def __len__(self):
        return len(self.cam_info)

class Camera:
    def __init__(self, client, camera_id):
        self.id = camera_id
        self.client = client

    def set_location(self, loc):
        self.client.request('vset /camera/{id}/location {x} {y} {z}'.format(id = self.id, **loc ))

    def set_rotation(self, rot):
        self.client.request('vset /camera/{id}/rotation {pitch} {yaw} {roll}'.format(id = self.id, **rot))

    def capture_depth(self):
        res = self.client.request('vget /camera/{id}/depth npy'.format(id = self.id))
        depth = read_npy(res)
        return depth

    def capture_img(self,name):
        res = self.client.request('vget /camera/{id}/lit {name}.png'.format(id = self.id,name = name ))
        img = read_png(res)
        return img

def CameraProcessYaw(v1,v2):
    Direction1 = (0, 1, 0)
    Direction2 = (v1[0]-v2[0],v1[1]-v2[1],v1[2]-v2[2])
    Direction2 = normalizeRows(Direction2)
    Angle = math.acos(dotproduct(Direction1, Direction2))
    Angle = Angle*(180/3.14159) 
    Direction3 = (1.0,1.0,1.0)
    Dot = dotproduct(Direction3, Direction2)
    if (Dot > 0.0):
      Angle *= -1
    print(Dot)
    return Angle
    
def angle(v1, v2):
    dx1 = v1[2] - v1[0]
    dy1 = v1[3] - v1[1]
    dx2 = v2[2] - v2[0]
    dy2 = v2[3] - v2[1]

    angle1 = math.atan2(dy1, dx1) # return radians of this point to (0, 0)
    angle1 = int(angle1 * 180/math.pi) # convert radian to degree
    #print(angle1)

    angle2 = math.atan2(dy2, dx2)
    angle2 = int(angle2 * 180/math.pi)
    #print(angle2)

    if angle1*angle2 >= 0: #TODO: 這個if-else有什麼屁用???
        included_angle = abs(angle1-angle2)
    else:
        included_angle = abs(angle1) + abs(angle2)
        if included_angle > 180:
            included_angle = 360 - included_angle

    return angle1



def dotproduct(v1, v2):
  return sum((a*b) for a, b in zip(v1, v2))

def length(v):
  return math.sqrt(dotproduct(v, v))

def angle1(v1, v2):
  return math.acos(dotproduct(v1, v2) / (length(v1) * length(v2)))		

def normalizeRows(x):
    """
    Implement a function that normalizes each row of the matrix x (to have unit length).
    
    Argument:
    x -- A numpy matrix of shape (n, m)
    
    Returns:
    x -- The normalized (by row) numpy matrix. You are allowed to modify x.
    """
    
    ### START CODE HERE ### (≈ 2 lines of code)
    # Compute x_norm as the norm 2 of x. Use np.linalg.norm(..., ord = 2, axis = ..., keepdims = True)
    x_norm = np.linalg.norm(x)
    
    # Divide x by its norm.
    x = x/x_norm
    ### END CODE HERE ###

    return x

def getNodeImage(i,j,loc,rot,moveVector,camera):

        if not os.path.isdir(outputDataPath + 'line/'):
                    os.mkdir(outputDataPath +'line/')
        if not os.path.isdir(outputDataPath +'image/'):
                os.mkdir(outputDataPath +'image/')

        shutil.rmtree(outputDataPath +'image/') # remove everything under directory
        shutil.rmtree(outputDataPath +'line/') # remove everything under directory
        location = [float(loc['x']),float(loc['y']),float(loc['z'])]
        location = location +  moveVector*10*(j) # set location to new position
        vector = dict(x= str(location[0]), y= str(location[1]), z= str(186) )
        camera.set_location(vector)
        #fileName = str(i)+'_'+str(j)+'_'+str(0)+'_'+str(vector['x'])+'_'+str(vector['y']) +'_'+str(rot['yaw'])
        #camera.capture_img(fileName)


        rotate = [float(rot['pitch']),float(rot['yaw']),float(rot['roll'])]
        rotDic = dict(pitch = str(0.0), yaw = str(rotate[1]), roll = str(0.0))
        print(rotate[1])
        camera.set_rotation(rotDic)
        fileName = outputDataPath + 'image/'+str(i)+'_'+str(j)+'_'+str(0)+'_'+str(vector['x'])+'_'+str(vector['y']) +'_'+str(rotate[1])
        camera.capture_img(fileName)
        filenameData=outputDataPath +'data/FileName/'+str(i)+".txt"
        with open(filenameData, 'a') as file:
            file.write(fileName+'\n' )
        return fileName
        

def sampleNode(i,j,loc,rot,moveVector,camera):

        if not os.path.isdir(outputDataPath +'line/'):
                os.mkdir(outputDataPath +'line/')
        if not os.path.isdir(outputDataPath +'image/'):
                os.mkdir(outputDataPath +'image/')

        shutil.rmtree(outputDataPath +'image/') # remove everything under directory
        shutil.rmtree(outputDataPath +'line/') # remove everything under directory
        location = [float(loc['x']),float(loc['y']),float(loc['z'])]
        location = location +  moveVector*10*(j)
        vector = dict(x= str(location[0]), y= str(location[1]), z= str(186) )
        camera.set_location(vector)
        #fileName = str(i)+'_'+str(j)+'_'+str(0)+'_'+str(vector['x'])+'_'+str(vector['y']) +'_'+str(rot['yaw'])
        #camera.capture_img(fileName)

        for k in range(0,41):
            rotate = [float(rot['pitch']),float(rot['yaw']),float(rot['roll'])]
            rotDic = dict(pitch = str(0.0), yaw = str(rotate[1]+(k-20)), roll = str(0.0))
            print(rotate[1],k-20)
            camera.set_rotation(rotDic)
            fileName = outputDataPath  + 'image/'+ str(i)+'_'+str(j)+'_'+str(k-20)+'_'+str(vector['x'])+'_'+str(vector['y']) +'_'+str(rotate[1]+(k-20))
            camera.capture_img(fileName)
            filenameData = outputDataPath +'data/FileName/'+str(i)+".txt"
            with open(filenameData, 'a') as file:
                file.write(fileName+'\n' )
        
        if not os.path.isdir(outputDataPath +'line/'):
                os.mkdir(outputDataPath +'line/')

        line.lineGenerator(outputDataPath +'image/',outputDataPath +'line/')
        CalculateHog.calculateHOG(outputDataPath +'line/',inputDataPath+'finialLine/',outputDataPath +'data/HOG/',i)
        CalculateFloorRatioVideo.run(outputDataPath +'image/',inputDataPath+'Seg/',outputDataPath +'data/Seg/')
        CalculateScoreVideo.run(outputDataPath +'data/Seg/',outputDataPath+'data/HOG/',outputDataPath +'data/finialScore/',i,j)
        
        #outputDataPath = 'D:/AutoImage/data/model_filled_afterUnreal/temp/'
        #inputDataPath = 'D:/AutoImage/data/model_filled_afterUnreal/'
def calculateTotalScore(dataPath,index,start,lr,output):
    ScorePath = open(dataPath + str(index)+".txt", 'r')
    startScore = 0
    leftScore = 0
    rightScore = 0

    countStart = 0
    countLeft = 0
    countRight = 0
    for lines in ScorePath:
        arr= lines.split(',')
        if int(arr[0]) == index and int(arr[1]) == start:
            startScore += float(arr[3])
            countStart += 1
        if int(arr[0]) == index and int(arr[1]) == start-lr:
            leftScore += float(arr[3])
            countLeft += 1
        if int(arr[0]) == index and int(arr[1]) == start+lr:
            rightScore += float(arr[3])
            countRight += 1
    print('startScore  '+str(start)+':'+str(startScore/countStart)+' ,leftScore  '+str(start-lr)+':'+str(leftScore/countLeft) + ',rightScore  '+str(start+lr)+':'+str(rightScore/countRight) )
    output = 'startScore '+str(start)+' :'+str(startScore/countStart)+' ,leftScore '+str(start-lr)+' :'+str(leftScore/countLeft) + ',rightScore '+str(start+lr)+' :'+str(rightScore/countRight)+'\n'
    filenameData=outputDataPath + 'data/LOG/'+str(index)+".txt"
    with open(filenameData, 'a') as file:
        file.write(output)

    if startScore/countStart > rightScore/countRight and leftScore/countLeft > startScore/countStart:
        return int(start + lr),output
    if startScore/countStart < rightScore/countRight and leftScore/countLeft < startScore/countStart:
        return int(start - lr) ,output
    if startScore/countStart < rightScore/countRight and leftScore/countLeft > startScore/countStart:
        return int(start),output
    if startScore/countStart > rightScore/countRight and leftScore/countLeft < startScore/countStart:
        return 1000,output

def calculateScore(dataPath,index,start,lr,output):
    '''
    Get average final score of start and range candidates
    '''
    ScorePath = open(dataPath + str(index)+".txt", 'r')
    startScore = 0
    rightScore = 0

    countStart = 0
    countRight = 0
    for lines in ScorePath:
        arr= lines.split(',')
        if int(arr[0]) == index and int(arr[1]) == start:
            startScore += float(arr[3])
            countStart += 1
        if int(arr[0]) == index and int(arr[1]) == start+lr:
            rightScore += float(arr[3])
            countRight += 1
    output = 'startScore '+str(start)+' :'+str(startScore/countStart) + ',rightScore '+ str(start+lr)+' :'+ str(rightScore/countRight)+'\n'
    filenameData=outputDataPath+'data/LOG/'+str(index)+".txt" # 產生data/LOG/檔案
    with open(filenameData, 'a') as file:
        file.write(output)
    return startScore/countStart,rightScore/countRight


def findRange(start,lr,sampleIndex,i,output,loc,rot,moveVector,camera,finalResult):
    '''
    這個function的功能應該是尋找最適合的偏移量
    '''
    sampleNode(i,start+lr,loc,rot,moveVector,camera)
    sampleNode(i,start,loc,rot,moveVector,camera)
    sampleIndex.append(start) #TODO: sampleIndex紀錄所有經過sampleNode()的positions
    sampleIndex.append(start+lr)
    rightResultList = []
    startResult,rightResult = calculateScore(outputDataPath+'data/finialScore/',i,start,lr,output)
    rightResultList.append(rightResult)
    finalLR = -1000
    isRange = -1
    isRange2 = -1

    while lr!=0:
        # rightIndex = rightResultList.index(min(rightResultList)) # 取得分數最小的index(應該是最佳的) 沒有使用到!!!!!!!!!
        imageName = getNodeImage(i,start+lr,loc,rot,moveVector,camera)
        print(imageName)
        floorImage = cv2.imread(imageName+'.png') # 取得新的unreal模擬照片
        sp = floorImage.shape
        width = sp[1]
        height = sp[0]
        if floorImage[height-10][int(width/2)][0] == 0: # 如果圖片中下點為黑色的
            lr = int(lr/1.2) # 應該是縮小偏移量的意思
        else:
            if rightResultList[len(rightResultList)-1] - startResult > 0.02: 
                '''
                如果rightResult中最後的分數比startResult分數還大0.02(對位較差)，進入這個case將lr縮小，
                如果縮小到(rightResultList[len(rightResultList)-1] - startResult < 0.005) 則進入下一個case
                '''
                if isRange == 1:
                    isRange2 = 1

                lr = int(lr/1.2) # 應該是縮小偏移量的意思
                if not start+lr in sampleIndex:
                    sampleNode(i,start+lr,loc,rot,moveVector,camera)
                    sampleIndex.append(start+lr)
                startResult,rightResult = calculateScore(outputDataPath+'data/finialScore/',i,start,lr,output)
                rightResultList.append(rightResult)
                finalLR = lr
            elif rightResultList[len(rightResultList)-1] - startResult < 0.005 and isRange2 != 1 and rightResultList[len(rightResultList)-1] - startResult > 0:
                '''
                從上一個case縮小到進入這個case之後，將isRange設為1(如果之後放大lr又進入上一個case的話就會把isRange2設為1，避免再次進入此case)。
                進入此case之後，若 position start+lr還沒有被search過則將finalLR = lr(目前最佳解)，再進入下一次迴圈
                '''
                isRange = 1
                llr= lr #llr = last lr????
                lr = int(lr*1.05)
                if abs(lr) <= 100:
                    if llr != lr: # lr稍大
                        if not start+lr in sampleIndex:
                            sampleNode(i,start+lr,loc,rot,moveVector,camera)
                            sampleIndex.append(start+lr)
                        startResult,rightResult = calculateScore(outputDataPath+'data/finialScore/',i,start,lr,output)
                        rightResultList.append(rightResult)
                        finalLR = lr
                    else: # lr很小 不再繼續search
                        finalLR = lr
                        lr = 0
                else:
                    if(lr<0):
                        finalLR = -100
                    else:
                        finalLR = 100
                    lr = 0 
            elif rightResultList[len(rightResultList)-1] - startResult < 0:
                if lr*1.2 >= 100:
                    finalLR = 100
                    lr = 0
                elif lr*1.2 <= -100 :
                    finalLR = -100
                    lr = 0
                else:
                    finalLR = int(lr*1.2)
                    lr = 0
                
            else:
                finalLR = lr
                lr = 0   
    filenameData=outputDataPath+'data/LOG/'+str(i)+".txt"
    out = 'result:' + str(finalLR)+"\n"     
    with open(filenameData, 'a') as file:
        file.write(out)

    return finalLR




def main():
    #playback_seq = PlaybackSequence(inputDataPath + 'demo8_path_4.json')
    playback_seq = PlaybackSequence(inputDataPath + '20190912_path.json') #TODO: 這是行車軌跡在unreal內的座標還是GPS座標??? 誰產生的???
    camera = Camera(client, 0)
    client.connect()
    res = client.request('vget /unrealcv/status')
    # The image resolution and port is configured in the config file.
    print(res)
    for i in range(0,500): #TODO: 500個GPS點，但其實這個json檔只有 64 個點，對應64個frame???
        sampleIndex = []
        output = ''
        start = 0
        lr = 100 #TODO: 'lr'是什麼意思

        #[loc,rot,moveVector] = playback_seq.get(i)
        #location = [float(loc['x']),float(loc['y']),float(loc['z'])]
        #print(location[0],location[1])

        [loc,rot,moveVector] = playback_seq.get(i)

        camera.set_location(loc)
        camera.set_rotation(rot)
        rightResult = -1000
        leftResult = -1000
        rightResult =  findRange(start,lr,sampleIndex,i,output,loc,rot,moveVector,camera,rightResult)
        leftResult = findRange(start,-lr,sampleIndex,i,output,loc,rot,moveVector,camera,leftResult)
		
        if rightResult == -1000 :
        	rightResult = 100
        elif leftResult == -1000 :
        	leftResult = -100
			
        for k in range(leftResult,rightResult):
            if not k in sampleIndex:
                sampleNode(i,k,loc,rot,moveVector,camera)
                sampleIndex.append(k)



if __name__ == '__main__':
    main()