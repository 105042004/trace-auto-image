import numpy as np
import argparse
import matplotlib.pyplot as plt
import cv2
from os import listdir
from os.path import isfile, isdir, join
import time
import multiprocessing
import threading

strLists = []
frame_id = 0

def calPixelRatioTask(args, lower):
  global strLists,frame_id
  files = args['files']
  mypath = args['mypath']
  segPath = args['segPath']
  weight = args['weight']
  for idx, f in enumerate(files):
    arr = f.split('_')
    frame_id = arr[0]
    img_seg = cv2.imread(segPath+str(arr[0])+'.png')
    img_floor = cv2.imread(mypath+str(f))
    output = calculatePixelRatio(img_seg, img_floor,weight)
    del img_seg
    del img_floor
    strList = str(arr[0])+","+str(arr[1]) +","+str(arr[2])+","+ str(output[0])+","+str(output[1])+","+str(output[2])+"\n"
    strLists[idx+int(lower)] = strList

def calculatePixelRatio(img_seg, img_floor,weight):
   
   w = int(img_seg.shape[0]/5)
   h = int(img_seg.shape[1]/5)
   img_seg = cv2.resize(img_seg,(h,w) , interpolation=cv2.INTER_CUBIC)
   img_floor = cv2.resize(img_floor, (h,w), interpolation=cv2.INTER_CUBIC)
   
   Gray_Color = [
        ([130,130,130], [140,140,140])#黄色范围~这个是我自己试验的范围，可根据实际情况自行调整~注意：数值按[b,g,r]排布
    ]

   blue_Color = [
        ([190,92,0], [210,112,10])#黄色范围~这个是我自己试验的范围，可根据实际情况自行调整~注意：数值按[b,g,r]排布
    ]

   purple_Color = [
        ([245,0,153], [255,10,173])#黄色范围~这个是我自己试验的范围，可根据实际情况自行调整~注意：数值按[b,g,r]排布
    ]
   
   sp = img_seg.shape
   width = sp[0]
   height = sp[1]
   countGray = 0
   countBlue = 0
   countPurple = 0
   countRed = 0
   allGray = 0
   weightBlue = 0.5
   weightPurple = 0.5
   weightRed = 1
   
   img_seg_crop = img_seg[int(width/2):width, 0:height]
   img_floor_crop = img_floor[int(width/2):width, 0:height]
   
   
   '''
   img_mask = (img_floor_crop != (0,0,0))
   #print(np.sum(img_mask)//3)
   target1 = (img_seg_crop == (140,140,140))
   target2 = (img_seg_crop == (200,102,0))
   target3 = (img_seg_crop == (255,0,163))
   target4 = (img_seg_crop == (120,120,180))
   allGray = np.sum(target1)//3
   countGray = np.sum(np.logical_and(target1, img_mask))//3
   countBlue = np.sum(np.logical_and(target2, img_mask))//3
   countPurple = np.sum(np.logical_and(target3, img_mask))//3
   countRed = np.sum(np.logical_and(target4, img_mask))//3
   '''
   #print(allGray_,countGray_,countBlue_,countPurple_,countRed_)
   mask_seg1 = cv2.inRange(img_seg_crop, np.asarray([140,140,140]),np.asarray([140,140,140]))
   mask_seg2 = cv2.inRange(img_seg_crop, np.asarray([200,102,0]),np.asarray([200,102,0]))
   mask_seg3 = cv2.inRange(img_seg_crop, np.asarray([255,0,163]),np.asarray([255,0,163]))
   mask_seg4 = cv2.inRange(img_seg_crop, np.asarray([120,120,180]),np.asarray([120,120,180]))
   mask_floor = 255 - cv2.inRange(img_floor_crop, np.asarray([0,0,0]),np.asarray([0,0,0]))
   allGray = int(np.sum(mask_seg1)/255)
   countGray = int(np.sum(np.bitwise_and(mask_floor,mask_seg1))/255)
   countBlue = int(np.sum(np.bitwise_and(mask_floor,mask_seg2))/255)
   countPurple = int(np.sum(np.bitwise_and(mask_floor,mask_seg3))/255)
   countRed = int(np.sum(np.bitwise_and(mask_floor,mask_seg4))/255)
   #print(allGray, countGray, countBlue, countPurple, countRed)
   '''
   allGray = 0
   countGray = 0
   countBlue = 0
   countPurple = 0
   countRed = 0
   for i in range(int(width/2),width):
       for j in range(0,height):
         data = img_seg[i, j]
         data1 = img_floor[i, j]
         if(data[0] ==140  and data[1] == 140 and data[2] == 140):
           allGray = allGray + 1
           if not (data1[0] ==0 and data1[1] == 0 and data1[2] == 0):
             countGray = countGray+1

         elif (data[0] ==200  and data[1] == 102 and data[2] == 0):
           if not (data1[0] ==0 and data1[1] == 0 and data1[2] == 0):
             countBlue = countBlue+1

         elif (data[0] ==255  and data[1] == 0 and data[2] == 163):
           if not (data1[0] ==0 and data1[1] == 0 and data1[2] == 0):
             countPurple = countPurple+1

         elif (data[0] ==120  and data[1] == 120 and data[2] == 180):
           if not (data1[0] ==0 and data1[1] == 0 and data1[2] == 0):
             countRed = countRed+1
   print(allGray, countGray, countBlue, countPurple, countRed)
   '''
   pixel = countGray + weightBlue*countBlue + weightPurple*countPurple- weightRed * countRed
   allPixel = allGray + countBlue + countPurple
   return [pixel,allPixel,pixel/allPixel]
   


def run(mypath,segPath,outputPath):
  # 讀取圖檔
 
  outputStr = ""
  # 取得所有檔案與子目錄名稱
  global strLists,frame_id
  files = listdir(mypath)
  num_threads = 12 # multiprocessing.cpu_count()
  ll = len(files)
  margin = ll/num_threads
  strLists = [str(x) for x in range(ll)]
  tasks = []
  for n in range(num_threads):
    task_arg = {}
    lower = margin * n
    upper = margin * (n+1) if margin * (n+1) < ll else ll
    files_split = [files[f] for f in range(int(lower), int(upper))]
    task_arg['files'] = files_split
    task_arg['mypath'] = mypath
    task_arg['segPath'] = segPath
    task_arg['weight'] = 0.5
    t = threading.Thread(target=calPixelRatioTask, args=(task_arg,lower))
    t.start()
    tasks.append(t)

  for t in tasks:
    t.join()

  for s in strLists:
    path = outputPath+str(frame_id)+'.txt'
    with open(path, 'a') as file:
        file.write(s)

  del strLists
  '''
  for f in range(0,len(files)):
    arr = files[f].split('_')
    #print(f)
    if True:
      img_seg = cv2.imread(segPath+str(arr[0])+'.png')
      img_floor = cv2.imread(mypath+str(files[f]))
      output = calculatePixelRatio(img_seg,img_floor,0.5)
      strList = str(arr[0])+","+str(arr[1]) +","+str(arr[2])+","+ str(output[0])+","+str(output[1])+","+str(output[2])+"\n"
      #print(strList)
      path = outputPath+str(arr[0])+'.txt'
      with open(path, 'a') as file:
        file.write(strList)
  '''
 
